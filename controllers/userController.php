<?php
session_start();
ini_set('display_errors', 1);

Class Users {
	private $db;

	public function __construct() {
		ob_start();    
    $this->db = Dbs::Conectar();
	}
	// public function __destruct() {
	//     $this->db = null;
	//     ob_end_flush();
	// }

	function save_user(){
		extract($_POST);
		$data = [];
		foreach($_POST as $k => $v){
			if(!in_array($k, array('id','cpass')) && !is_numeric($k)){
				if($k =='password')
					$v = password_hash($v, PASSWORD_DEFAULT);
				if(empty($data)){
					$data[] = "$v";
				}else{
					$data[] = "$v";
				}
			}
		}

		
		if($data[0] == '' or $data[1] == '' or $data[2] == '' or $data[3] == '' or $data[4] == '' or $data[5] == ''){
			return 4;
			$this->db = null;
		}else if(filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == false){
			return 5;
		}else{
			$checkEmail = $this->db->query("SELECT * FROM usuarios_admin where email = '$data[4]' ")->rowCount();
			$checkUsername = $this->db->query("SELECT * FROM usuarios_admin where username = '$data[2]' ")->rowCount();
	
			if($checkEmail > 0){
				return 2;
				exit;
			}elseif($checkUsername> 0){
				return 3;
				exit;
			}else{
				$save = $this->db->query("INSERT INTO `usuarios_admin` (`id`,`nombres`,`apellidos`,`username`,`email`,`password`,`rol`) VALUES (NULL, '$data[0]','$data[1]','$data[2]','$data[4]','$data[5]',$data[3])");
				return 1;
			}
			$this->db = null;
		}
	}

	function update_user(){
		extract($_POST);
		$data = [];
		foreach($_POST as $k => $v){
			if($k =='password')
				if($v != '')
					$v = password_hash($v, PASSWORD_DEFAULT);
			$data[] = "$v";
		}

		if($data[0] == '' or $data[1] == '' or $data[2] == '' or $data[3] == '' or $data[4] == '' or $data[5] == ''){
			return 4;
			$this->db = null;
		}else if(filter_var($data[5], FILTER_VALIDATE_EMAIL) == false){
			return 5;
		}else if($data[6] == ''){
			$checkUser = $this->db->query("SELECT * FROM usuarios_admin where id = '$data[0]' ")->fetchObject();
			$checkEmail = $this->db->query("SELECT * FROM usuarios_admin where email = '$data[5]' ")->fetchObject();
			$checkUsername = $this->db->query("SELECT * FROM usuarios_admin where username = '$data[3]' ")->fetchObject();
	
			if(!$checkEmail and !$checkUsername){
				$save = $this->db->prepare("UPDATE usuarios_admin SET `username`=?,
					 `nombres`=?,`apellidos`=?,`email`=?,`rol`=? where id =?");
				$save->execute([$data[3],$data[1],$data[2],$data[5],$data[4],$data[0]]);
				return 1;
	
			}elseif($checkEmail){
				if($checkEmail->{'id'} != $checkUser->{'id'}){
					return 2;
					$this->db = null;
					exit;
				}elseif($checkUsername){
					if($checkUsername->{'id'} != $checkUser->{'id'}){
						return 3;
						$this->db = null;
						exit;
					}else{
						$save = $this->db->prepare("UPDATE usuarios_admin SET `username`=?,
						 `nombres`=?,`apellidos`=?,`email`=?,`rol`=? where id =?");
						$save->execute([$data[3],$data[1],$data[2],$data[5],$data[4],$data[0]]);
						return 1;
						$this->db = null;
					}
				}else{
					$save = $this->db->prepare("UPDATE usuarios_admin SET `username`=?,
						 `nombres`=?,`apellidos`=?,`email`=?,`rol`=? where id =?");
					$save->execute([$data[3],$data[1],$data[2],$data[5],$data[4],$data[0]]);
					return 1;
					$this->db = null;
				}
			}elseif($checkUsername->{'id'} != $checkUser->{'id'}){
				return 3;
				$this->db = null;
				exit;
			}else{
				$save = $this->db->prepare("UPDATE usuarios_admin SET `username`=?,
					`nombres`=?,`apellidos`=?,`email`=?,`rol`=? where id =?");
				   $save->execute([$data[3],$data[1],$data[2],$data[5],$data[4],$data[0]]);
				return 1;
				$this->db = null;
			}
			// return "que pasa";
		}else{
			$checkUser = $this->db->query("SELECT * FROM usuarios_admin where id = '$data[0]' ")->fetchObject();
			$checkEmail = $this->db->query("SELECT * FROM usuarios_admin where email = '$data[5]' ")->fetchObject();
			$checkUsername = $this->db->query("SELECT * FROM usuarios_admin where username = '$data[3]' ")->fetchObject();
	
			if(!$checkEmail and !$checkUsername){
				$save = $this->db->prepare("UPDATE usuarios_admin SET `username`=?,
					 `nombres`=?,`apellidos`=?,`email`=?,`password`=?,`rol`=? where id =?");
				$save->execute([$data[3],$data[1],$data[2],$data[5],$data[6],$data[4],$data[0]]);
				return 1;
				$this->db = null;
			}elseif($checkEmail){
				if($checkEmail->{'id'} != $checkUser->{'id'}){
					return 2;
					$this->db = null;
					exit;
				}elseif($checkUsername){
					if($checkUsername->{'id'} != $checkUser->{'id'}){
						return 3;
						$this->db = null;
						exit;
					}else{
						$save = $this->db->prepare("UPDATE usuarios_admin SET `username`=?,
							`nombres`=?,`apellidos`=?,`email`=?,`password`=?,`rol`=? where id =?");
						$save->execute([$data[3],$data[1],$data[2],$data[5],$data[6],$data[4],$data[0]]);
						return 1;
						$this->db = null;
					}
				}else{
					$save = $this->db->prepare("UPDATE usuarios_admin SET `username`=?,
						 `nombres`=?,`apellidos`=?,`email`=?,`password`=?,`rol`=? where id =?");
					$save->execute([$data[3],$data[1],$data[2],$data[5],$data[6],$data[4],$data[0]]);
					return 1;
					$this->db = null;
				}
			}elseif($checkUsername->{'id'} != $checkUser->{'id'}){
				return 3;
				$this->db = null;
				exit;
			}else{
				$save = $this->db->prepare("UPDATE usuarios_admin SET `username`=?,
					`nombres`=?,`apellidos`=?,`email`=?,`password`=?,`rol`=? where id =?");
				   $save->execute([$data[3],$data[1],$data[2],$data[5],$data[6],$data[4],$data[0]]);
				return 1;
				$this->db = null;
			}
		}
	}

	function delete_user(){
		extract($_POST);

		$delete = $this->db->query("DELETE FROM usuarios_admin where id = '$id' ");
		if($delete)
			return 1;
		$this->db = null;
	}

	

}