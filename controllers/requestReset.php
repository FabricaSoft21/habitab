<?php
// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '../../vendor/autoload.php';

require '../../config/Conexion.php';

$Db = Dbs::Conectar();
if (isset($_POST['email'])) {


    $emailTo = $_POST['email']; 
    
    
    $searchUser = $Db->query("SELECT * FROM usuarios_admin WHERE email='$emailTo'")->rowCount(); 
   
   if (filter_var($_POST['email'], FILTER_VALIDATE_EMAIL) == false){
    $codigo = '<p style="color:red;">Ups, parece que no es una direccion de email valida, ejemplo: prueba@ejemplo.com.</p>';
    header("location: recuperarContrasena.php?codigo=$codigo");
   }elseif ($searchUser == 0) {
    $codigo = '<p style="color:red;">Correo electrónico no existe.</p>';
    header("location: recuperarContrasena.php?codigo=$codigo");
   }else{
    $mail = new PHPMailer(true);    
    $date_creation = date('d-m-Y', time());
    $fecha = new DateTime(); 
    $fecha->modify('+24 hours'); 
    $date_expiration = $fecha->format('d-m-Y');
    $code = uniqid(true); // true for more uniqueness 
    $query = $Db->query("INSERT INTO reset_passwords (code, email,date_creation,date_expiration) VALUES('$code','$emailTo','$date_creation','$date_expiration')"); 
    if (!$query) {
       exit('Error'); 
    }
    try {
        //Server settings
        $mail->SMTPDebug = 0;     // Enable verbose debug output, 1 for produciton , 2,3 for debuging in devlopment 
        $mail->isSMTP();                                      // Set mailer to use SMTP
        $mail->Host = 'smtp.gmail.com';  // Specify main and backup SMTP servers
        $mail->SMTPAuth = true;                               // Enable SMTP authentication
        $mail->Username = 'tucorreo@gmail.com';                 // SMTP username
        $mail->Password = 'tucontraseña';                           // SMTP password
        // $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        $mail->SMTPSecure = 'tls';                            // Enable TLS encryption, `ssl` also accepted
        // $mail->Port = 587;   // for tls                                 // TCP port to connect to
        $mail->Port = 587;

        //Recipients
        $mail->setFrom('email@gmail.com', 'Habitat y la construccion'); // from who? 
        $mail->addAddress($emailTo, 'Habitat y la construccion');     // Add a recipient

        $mail->addReplyTo('no-replay@example.com', 'No Replay');
        // $mail->addCC('cc@example.com');
        // $mail->addBCC('bcc@example.com');

        //Content
        // this give you the exact link of you site in the right page 
        // if you are in actual web server, instead of http://" . $_SERVER['HTTP_HOST'] write your link 
        $url = "https://" . $_SERVER['HTTP_HOST'] . dirname($_SERVER['PHP_SELF']). "/resetContrasena.php?code=$code"; 
        $mail->isHTML(true);                                  // Set email format to HTML
        $mail->Subject = 'Restablecer Password';
        $mail->Body    = "
        <html>
        <head>
        <style type='text/css'>
        @font-face {
          font-family: 'Open Sans';
          font-style: normal;
          font-weight: 400;
          src: local('Open Sans'), local('OpenSans'), url(http://themes.googleusercontent.com/static/fonts/opensans/v6/cJZKeOuBrn4kERxqtaUH3T8E0i7KZn-EPnyo3HZu7kw.woff) format('woff');
        }
        body {
            color: #333;
            font-family: 'Open Sans', sans-serif;
            margin: 0px;
            font-size: 16px;
        }
        .pie {
            font-size:12px;
            color:#999797;
        }
        .centro {
            font-size:16px;
        }
        .centro a{
            text-decoration:none;
            color: #0487b8;
        }
        .centro a:hover{
            text-decoration: underline;
            color: #0487b8;
        }
        </style>
        </head>
        <body>
        <table width='593' height='324' border='0' align='center'>
          <tr>
            <td>&nbsp;</td>
          </tr>
          <tr>
            <td style='text-align: center;' height='88'><img src='https://ape.sena.edu.co/imgLayout/logos/LogoSENA-naranja_vector.png' width='350' height='200' /></td>
          </tr>
          <tr>
            <td height='97' valign='top' class='centro'><h3>Recuperación de contraseña
            </h3>
           Para recuperar tu contraseña haz <a href='$url'>click aquí</a> para redireccionarte, tiene un máximo de 24 horas para realizar el cambio</td>
          </tr>
          <br>
          <tr>
            <td height='60' >Si no funciona el redireccionamiento puedes copiar el siguiente link: $url</td>
          </tr>
          <tr>
            <td height='27' class='pie'>Este email es una notificaci&oacute;n autom&aacute;tica</td>
          </tr>
        </table>
        </body>
        </html>
        ";
        $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

        // to solve a problem 
        $mail->SMTPOptions = array(
            'ssl' => array(
            'verify_peer' => false,
            'verify_peer_name' => false,
            'allow_self_signed' => true
            )
        );


        $mail->send();
        $codigo = '<p>Se ha enviado el mensaje a tu correo.</p>';
	    header("location: recuperarContrasena.php?codigo=$codigo"); 
    } catch (Exception $e) {
        echo ' Error al enviar el mensage: ', $mail->ErrorInfo;
    }
}
    exit(); // to stop user from submitting more than once 
}
