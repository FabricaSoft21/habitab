<?php
include '../../../config/session.php';

extract($_POST);

if (isset($_POST['readrecords'])) {

    $data =  '<table  id="datatable"  name="datatable" class="table table-bordered table-striped ">
						<tr class="bg-dark text-white">
							<th>No.</th>
							<th>Nombre Campaña</th>
							<th>Url Formulario</th>
							<th>Url Resultados</th>
							<th>Copiar URL</th> 
							<th>Descripción</th>';
    if ($_SESSION["rol"] == '1') {
        $data .= '<th>Estado</th> 
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>';
    } else {
        $data .= '</tr>';
    }

    $Db = Dbs::Conectar();
    $displayquery = "SELECT * FROM `campana_maestra` where estado='1' order by id desc";
    $result = $Db->query($displayquery);
    if ($result->rowCount() > 0) {
        //"6"=>($number)?'<span >Activado</span>':'<span >Desactivado</span>'
        $number = 1;
        while ($row = $result->fetch()) {
            $separacion = explode("/", $row["link_f"]);
            $codigo="javascript:getlink('$separacion[5]')";

            $data .= '<tr>  
            <td>' . $number . '</td>
            <td>' . $row['nombre_f'] . '</td>
            <td> <a  href="' . $row['link_f'] . '"class="btn btn-warning btn-lg active"
                                       role="button"
                                       aria-pressed="true" data-toggle="tooltip" data-placement="right"
                                       title="Ir al formulario" target="_blank"><i class="fa fa-eye"></i>
                                    </a></td>
            <td><a href="' . $row['link_r'] . '" class="btn btn-success btn-lg active"
                                       role="button"
                                       aria-pressed="true" data-toggle="tooltip" data-placement="right"
                                       title="Ver Resultados" target="_blank"><i class="fa fa-table"></i>
                                    </a></td>
            <td><a  href="' . $codigo . '" class="btn btn-info btn-lg active" role="button" aria-pressed="true" data-toggle="tooltip" data-placement="right"
                        title="Copiar url"><i class="fa fa-files-o"></i></a></td>
            <td>' . $row['descripcion'] . '</td>
            ';
            if ($_SESSION["rol"] == '1') {
                $data .= '<td><button onclick="DeleteUser2(' . $row['id'] . ')"  class="btn btn-danger" id="Btnestado">Desactivar</button>
                    <td><button class="btn btn-outline-info btn-lg" id="BtnEditar"
                                            value="editar" data-toggle="modal" data-target="#update_user_modal"
                                            data-id="' . $row['id'] . '" data-name="' . $row['nombre_f'] . '" data-urlf="' . $row['link_f'] . '"
                                            data-urlr="' . $row['link_r'] . '" data-descripcion="' . $row['descripcion'] . '">
                                        <i class="fa fa-edit"></i>
                                    </button></td>
                    <td><button onclick="DeleteUser(' . $row['id'] . ')"  class="btn btn-outline-danger btn-lg" id="BtnInhablitar">
                                        <i class="fa fa-trash-o"></i>
                    </button></td>
                    </tr>';
            } else {
                '</tr>';
            }
            $number++;
        }
    }
    $data .= '</table>';
    echo $data;
    $result = null;
    $Db = null;
}


if (isset($_POST['readrecords2'])) {

    $data =  '<table  id="datatable"  name="datatable" class="table table-bordered table-striped ">
						<tr class="bg-dark text-white">
							<th>No.</th>
							<th>Nombre Campaña</th>
							<th>Url Formulario</th>
							<th>Url Resultados</th>
							<th>id</th> 
							<th>Descripción</th>';
    if ($_SESSION["rol"] == '1') {
        $data .= '<th>Estado</th> 
                <th>Editar</th>
                <th>Eliminar</th>
            </tr>';
    } else {
        $data .= '</tr>';
    }

    $Db = Dbs::Conectar();
    $displayquery = "SELECT * FROM `campana_maestra` where estado='0' order by id desc";
    $result = $Db->query($displayquery);
    if ($result->rowCount() > 0) {

        //"6"=>($number)?'<span >Activado</span>':'<span >Desactivado</span>'

        $number = 1;
        while ($row = $result->fetch()) {

            $data .= '<tr>  
				<td>' . $number . '</td>
				<td>' . $row['nombre_f'] . '</td>
				<td> <a  href="' . $row['link_f'] . '"class="btn btn-warning btn-lg active"
                                           role="button"
                                           aria-pressed="true" data-toggle="tooltip" data-placement="right"
                                           title="Ir al formulario" target="_blank"><i class="fa fa-eye"></i>
                                        </a></td>
				<td><a href="' . $row['link_r'] . '" class="btn btn-success btn-lg active"
                                           role="button"
                                           aria-pressed="true" data-toggle="tooltip" data-placement="right"
                                           title="Ver Resultados" target="_blank"><i class="fa fa-table"></i>
                                        </a></td>
				<td>' . $row['id'] . '</td>
				<td>' . $row['descripcion'] . '</td>
                ';
            if ($_SESSION["rol"] == '1') {
                $data .= '<td><button onclick="DeleteUser3(' . $row['id'] . ')"  class="btn btn-info" id="Btnestado">Activar</button>
                        <td><button class="btn btn-outline-info btn-lg" id="BtnEditar"
                                                value="editar" data-toggle="modal" data-target="#update_user_modal"
                                                data-id="' . $row['id'] . '" data-name="' . $row['nombre_f'] . '" data-urlf="' . $row['link_f'] . '"
                                                data-urlr="' . $row['link_r'] . '" data-descripcion="' . $row['descripcion'] . '">
                                            <i class="fa fa-edit"></i>
                                        </button></td>
                        <td><button onclick="DeleteUser(' . $row['id'] . ')"  class="btn btn-outline-danger btn-lg" id="BtnInhablitar">
                                            <i class="fa fa-trash-o"></i>
                        </button></td>
                        </tr>';
            } else {
                '</tr>';
            }
            $number++;
        }
    }
    $data .= '</table>';
    echo $data;
    $result = null;
    $Db = null;
}


//adding records in database el formulario de la campaña
/*if (isset($_POST['nombre_f']) &&  isset($_POST['link_f']) && isset($_POST['link_r']) && isset($_POST['descripcion'])) {
    $query = " INSERT INTO `posible_tabla_formularios`( `link_f`, `link_r`, `nombreFormulario`, `descripcion`, `estado`) VALUES('$nombreFormulario', '$urlFormulario', '$urlRespuestas', '$descripcion', '1' )   ";

    if ($result = mysqli_query($conn, $query)) {
        exit(mysqli_error());
    } else {
        echo "1 record added";
    }
}*/


// pass id on modal
if (isset($_POST['id']) && isset($_POST['id']) != "") {
    $user_id = $_POST['id'];
    $Db = Dbs::Conectar();
    $query = "SELECT * FROM campana_maestra WHERE id = '$user_id'";
    $result = $Db->query($query);



    $response = array();

    if ($result->rowCount() > 0) {
        while ($row = $result->fetch()) {

            $response = $row;
        }
    }
    // agar ek bhi value nai milta hai tho data not found no. of rows 0 hai tho
    else {
        $response['status'] = 200;
        $response['message'] = "Data not found!";
    }
    //     PHP has some built-in functions to handle JSON.
    // Objects in PHP can be converted into JSON by using the PHP function json_encode(): 

    echo json_encode($response);
}
// ye top wala id jo humhe mil raha hai uska hai jaha wo id check karega sahi hai ya nai agar nai tho invalid req boldega...
else {
    $response['status'] = 200;
    $response['message'] = "Invalid Request!";
}
//////////////// update table//////////////

if (isset($_POST['hidden_user_id'])) {
    // get values

    //    nombreFormularioe : nombreFormularioe,
    // urlFormularioe : urlFormularioe,
    //   urlRespuestase : urlRespuestase,
    //  descripcione : descripcione 



    $hidden_user_id = $_POST['hidden_user_id'];
    $nombreFormularioe = $_POST['nombre_fe'];
    $urlFormularioe = $_POST['link_fe'];
    $urlRespuestase = $_POST['link_re'];
    $descripcione = $_POST['descripcione'];

    $Db = Dbs::Conectar();
    $query = "UPDATE campana_maestra SET link_f = '$urlFormularioe', link_r = '$urlRespuestase', nombre_f = '$nombreFormularioe', descripcion = '$descripcione'  WHERE id = '$hidden_user_id'";
    if (!$result = $Db->query($query)) {
        echo $e->getMessage();
    }
}
/////////////Delete user record /////////

if (isset($_POST['deleteid'])) {

    $user_id = $_POST['deleteid'];

    $Db = Dbs::Conectar();
    $query = "delete from campana_maestra where id ='$user_id'";
    if (!$result = $Db->query($query)) {
        echo $e->getMessage();
    }
}



if (isset($_POST['deleteid2'])) {

    $user_id = $_POST['deleteid2'];


    $Db = Dbs::Conectar();
    $query = "UPDATE campana_maestra SET estado='0' where id ='$user_id' ";
    if (!$result = $Db->query($query)) {
        echo $e->getMessage();
    }
}



if (isset($_POST['deleteid3'])) {

    $user_id = $_POST['deleteid3'];


    $Db = Dbs::Conectar();
    $query = "UPDATE campana_maestra SET estado='1' where id ='$user_id' ";
    if (!$result = $Db->query($query)) {
        echo $e->getMessage();
    }
}
