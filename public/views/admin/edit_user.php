<?php include '../../../config/session.php';

if($_SESSION['rol'] != '1'){
	header('location: home.php');
}

$Db = Dbs::Conectar();
$query = $Db->query("SELECT * FROM usuarios_admin where id = ".$_GET['id'])->fetch(PDO::FETCH_NUM);

?>

<!doctype html>
<html lang="es">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css"
          integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN"
          crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	
	<script src="../../js/jquery.min.js"></script>
	<link rel="stylesheet" href="../../css/validate_password.css">
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css"/>
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
</head>
<body>
    
<?php include 'includes/navbar.php'; ?>

<div style="padding:10px;"></div>
<div class="col-lg-12">
	<div class="card">
		<div class="card-body">
			<form action="" id="manage_user">
				<input type="hidden" name="id" value="<?php echo isset($query[0]) ? $query[0] : '' ?>">
				<div class="row">
					<div class="col-md-6 border-right">
						<b class="text-muted">Informacion Personal</b>
						<div class="form-group">
							<label for="" class="control-label">Nombres</label>
							<input type="text" id="name" name="nombres" class="form-control form-control-sm" value="<?php echo isset($query[2]) ? $query[2] : '' ?>">
							<small id="err_name" data-status=''></small>
						</div>
						<div class="form-group">
							<label for="" class="control-label">Apellidos</label>
							<input type="text" id="lastName" name="apellidos" class="form-control form-control-sm"  value="<?php echo isset($query[3]) ? $query[3] : '' ?>">
							<small id="err_lastName" data-status=''></small>
						</div>
						<div class="form-group">
							<label for="" class="control-label">Username</label>
							<input type="text" id="username" name="username" class="form-control form-control-sm" value="<?php echo isset($query[1]) ? $query[1] : '' ?>">
							<small id="err_username" data-status=''></small>
						</div>
					</div>
					<div class="col-md-6">
						<b class="text-muted">Credenciales del sistema</b>
				
						<div class="form-group">
							<label for="" class="control-label">Rol</label>
							<select name="rol" id="rol" class="custom-select custom-select-sm">
								
								<option value="2" <?php echo isset($query[6]) && $query[6] == 2 ? 'selected' : '' ?>>usuario</option>
								<option value="1" <?php echo isset($query[6]) && $query[6] == 1 ? 'selected' : '' ?>>Administrador</option>
							</select>
						</div>
					
						<div class="form-group">
							<label class="control-label">Email</label>
							<input type="text" id="email" class="form-control form-control-sm" name="email" value="<?php echo isset($query[4]) ? $query[4] : '' ?>">
							<small id="err_email"></small>
						</div>
						<!-- prueba contraseña -->
						<label class="control-label" >Contraseña</label>
						<div class="center">
						<div class="form">
							<div class="form-element">
								
							<input type="password" class="form-control form-control-sm" id="password" name="password" autocomplete="off">
							<small><i><?php echo isset($query[0]) ? "Dejar en blanco si no va a cambiar la contraseña ":'' ?></i></small>
							<div class="toggle-password">
								<i class="fa fa-eye" style="color: grey;"></i>
								<i class="fa fa-eye-slash" style="color: grey;"></i>
							</div>
							<div class="password-policies">
								<div class="policy-length">
								Mínimo 8 caracteres
								</div>
								<div class="policy-number">
								Mínimo un número
								</div>
								<div class="policy-uppercase">
								Mínimo una mayúscula
								</div>
								<div class="policy-special">
								Al menos un carácter especial
								</div>
							</div>
							</div>
							
						</div>
						</div>
						<!-- finaliza prueba contraseña -->

						<!-- prueba contraseña -->
						<label class="control-label" >Confirmar Contraseña</label>
						<div class="center">
						<div class="form">
							<div class="form-element">
								
							<input type="password" class="form-control form-control-sm" id="cpass" name="cpass" >
							<div class="toggle-passwords">
								<i class="fa fa-eye" style="color: grey;"></i>
								<i class="fa fa-eye-slash" style="color: grey;"></i>
							</div>
							
							</div>
							
						</div>
						<small id="pass_match" data-status=''></small>
						</div>
						<!-- finaliza prueba contraseña -->
					</div>
				</div>
				<hr>
				<div class="col-lg-12 text-right justify-content-center d-flex">
					<button class="btn btn-primary mr-2" id="guardar">Guardar</button>
					<button class="btn btn-secondary" type="button" onclick="location.href = 'user_list.php?page=user_list'">Cancelar</button>
				</div>
			</form>
		</div>
	</div>
</div>
<script>
	$('[name="password"],[name="cpass"]').keyup(function(){
		var pass = $('[name="password"]').val()
		var cpass = $('[name="cpass"]').val()
		if(cpass == '' ||pass == ''){
			$('#pass_match').attr('data-status','')
		}else{
			if(cpass == pass){
				$('#pass_match').attr('data-status','1').html('<i class="text-success">Contraseña coincide.</i>')
			}else{
				$('#pass_match').attr('data-status','2').html('<i class="text-danger">La Contraseña no coincide.</i>')
			}
		}
	})
	function displayImg(input,_this) {
	    if (input.files && input.files[0]) {
	        var reader = new FileReader();
	        reader.onload = function (e) {
	        	$('#cimg').attr('src', e.target.result);
	        }

	        reader.readAsDataURL(input.files[0]);
	    }
	}
	$('#manage_user').submit(function(e){
		e.preventDefault()
		$('input').removeClass("border-danger")
		start_load()
		$('#msg').html('')
		if($('#pass_match').attr('data-status') != 1){
			if($("[name='password']").val() !=''){
				$('[name="password"],[name="cpass"]').addClass("border-danger")
				end_load()
				return false;
			}
		}

		let name = $('#name').val()
		let lastName = $('#lastName').val()
		let username = $('#username').val()
		let email = $('#email').val()
		let rol = $('#rol').val()
		let pass = $('#password').val()
		let cpass = $('#cpass').val()

		$('#guardar').prop('disabled', true);

		if(name == ''){
			$('#err_name').html("<i style='color: red;'>el campo nombre no puede ir vacío</i>");
			$('#err_name').show();
			$('#guardar').prop('disabled', false);

			setTimeout(function(){
				$('#err_name').hide();
			},2000)
		}else if(lastName == ''){
			$('#err_lastName').html("<i style='color: red;'>el campo apellidos no puede ir vacío</i>");
			$('#err_lastName').show();
			$('#guardar').prop('disabled', false);

			setTimeout(function(){
				$('#err_lastName').hide();
			},2000)
		}else if(username == ''){
			$('#err_username').html("<i style='color: red;'>el campo username no puede ir vacío</i>");
			$('#err_username').show();
			$('#guardar').prop('disabled', false);

			setTimeout(function(){
				$('#err_username').hide();
			},2000)
		}else if(email == ''){
			$('#err_email').html("<i style='color: red;'>el campo email no puede ir vacío</i>");
			$('#err_email').show();
			$('#guardar').prop('disabled', false);

			setTimeout(function(){
				$('#err_email').hide();
			},2000)
		}else if(pass == '' && cpass == ''){
			$.ajax({
				url:'ajax.php?action=update_user',
				data: new FormData($(this)[0]),
				cache: false,
				contentType: false,
				processData: false,
				method: 'POST',
				type: 'POST',
				success:function(resp){
					if(resp == 1){
						$('#msg').html("<div class='alert alert-success' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Usuario actualizado exitosamente.</div>");
						setTimeout(function(){
							location.replace('user_list.php?page=user_list')
						},750)
					}else if(resp == 2){
						$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Correo en uso.</div>");
						$('#msg').show();
						$('[name="email"]').addClass("border-danger")
						$('#guardar').prop('disabled', false);
						setTimeout(function(){
							$('#msg').hide();
						},3000)
						end_load()
					}else if(resp == 3){
						$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Username en uso.</div>");
						$('#msg').show();
						$('[name="username"]').addClass("border-danger")
						$('#guardar').prop('disabled', false);
						setTimeout(function(){
							$('#msg').hide();
						},3000)
						end_load()
					}else if(resp == 4){
						$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Todos los campos son obligatorios.</div>");
						$('#msg').show();
						$('#guardar').prop('disabled', false);
						setTimeout(function(){
							$('#msg').hide();
						},3000)
						end_load()
					}else if(resp == 5){
						$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Ups, parece que no es una direccion de email valida, ejemplo: prueba@ejemplo.com.</div>");
						$('#msg').show();
						$('#guardar').prop('disabled', false);
						setTimeout(function(){
							$('#msg').hide();
						},3000)
						end_load()
					}
				}
			})
		}else{
			var myregex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
			if( myregex.test(pass) & myregex.test(cpass)){
				$.ajax({
					url:'ajax.php?action=update_user',
					data: new FormData($(this)[0]),
					cache: false,
					contentType: false,
					processData: false,
					method: 'POST',
					type: 'POST',
					success:function(resp){
						if(resp == 1){
							$('#msg').html("<div class='alert alert-success' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Usuario actualizado exitosamente.</div>");
							setTimeout(function(){
								location.replace('user_list.php?page=user_list')
							},750)
						}else if(resp == 2){
							$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Correo en uso.</div>");
							$('#msg').show();
							$('[name="email"]').addClass("border-danger")
							$('#guardar').prop('disabled', false);
							setTimeout(function(){
								$('#msg').hide();
							},3000)
							end_load()
						}else if(resp == 3){
							$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Username en uso.</div>");
							$('#msg').show();
							$('[name="username"]').addClass("border-danger")
							$('#guardar').prop('disabled', false);
							setTimeout(function(){
								$('#msg').hide();
							},3000)
							end_load()
						}else if(resp == 4){
							$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Todos los campos son obligatorios.</div>");
							$('#msg').show();
							$('#guardar').prop('disabled', false);
							setTimeout(function(){
								$('#msg').hide();
							},3000)
							end_load()
						}else if(resp == 5){
							$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;'>Ups, parece que no es una direccion de email valida, ejemplo: prueba@ejemplo.com.</div>");
							$('#msg').show();
							$('#guardar').prop('disabled', false);
							setTimeout(function(){
								$('#msg').hide();
							},3000)
							end_load()
						}
					}
				})
			}else{
				$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px; margin-top: 17px;'>Elige una contraseña más segura.</div>");
				$('#msg').show();
				$('[name="password"]').addClass("border-danger")
				$('[name="cpass"]').addClass("border-danger")
				$('#guardar').prop('disabled', false);
				setTimeout(function(){
					$('#msg').hide();
				},3000)
			}
		}
	})
</script>
<script>

function _id(name){
  return document.getElementById(name);
}
function _class(name){
  return document.getElementsByClassName(name);
}
_class("toggle-password")[0].addEventListener("click",function(){
  _class("toggle-password")[0].classList.toggle("active");
  if(_id("password").getAttribute("type") == "password"){
    _id("password").setAttribute("type","text");
  } else {
    _id("password").setAttribute("type","password");
  }
});

_class("toggle-passwords")[0].addEventListener("click",function(){
  _class("toggle-passwords")[0].classList.toggle("active");
  if(_id("cpass").getAttribute("type") == "password"){
    _id("cpass").setAttribute("type","text");
  } else {
    _id("cpass").setAttribute("type","password");
  }
});



_id("password").addEventListener("focus",function(){
  _class("password-policies")[0].classList.add("active");
});
_id("password").addEventListener("blur",function(){
  _class("password-policies")[0].classList.remove("active");
});

_id("password").addEventListener("keyup",function(){
  let password = _id("password").value;
  
  if(/[A-Z]/.test(password)){
    _class("policy-uppercase")[0].classList.add("active");
  } else {
    _class("policy-uppercase")[0].classList.remove("active");
  }
  
  if(/[0-9]/.test(password)){
    _class("policy-number")[0].classList.add("active");
  } else {
    _class("policy-number")[0].classList.remove("active");
  }
  
  if(/[^A-Za-z0-9]/.test(password)){
    _class("policy-special")[0].classList.add("active");
  } else {
    _class("policy-special")[0].classList.remove("active");
  }
  
  if(password.length > 7){
    _class("policy-length")[0].classList.add("active");
  } else {
    _class("policy-length")[0].classList.remove("active");
  }

  

});

function checkPassword(password){
    var myregex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/; 
   if(myregex.test(password)){
       alert(password+" es valido :-) !");
       return true;        
   }else{
      alert(password+" NO es valido!");
       return false;        
   }   
 }

  function checkForm(form){
    
    if(form.password.value != "" && form.password.value == form.cpass.value) {
      if(!checkPassword(form.password.value)) {
        alert("La contraseña no es valida!");
        form.password.focus();
        return false;
      }
    } else {
      alert("Error: las contraseñas no coinciden!");
      form.password.focus();
      return false;
    }
    return true;
  }
</script>

<div id="msg" 
</div>

<?php include 'footer.php' ?>