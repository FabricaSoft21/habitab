<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
  <a class="navbar-brand" href="#">Hábitat y Construcción</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse pl-auto" id="navbarTogglerDemo02">
    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
      <li class="nav-item">
        <a class="nav-link" href="home.php">Campañas</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="GoogleUser.php">Google API</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="FacebookUser.php">Facebook API</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="resultados/callback.php">Resultados</a>
      </li>
      <?php 
      if($_SESSION['rol'] == '1'){ ?>
      <li class="nav-item">
        <a class="nav-link" href="user_list.php">Usuarios</a>
      </li>
      <?php } ?>  
      &nbsp;
      <li class="nav-item">
        <a class="btn btn-outline-success my-2 my-sm-0" href="logout.php">salir <i class="fa fa-sign-out"></i></a>
      </li>
    </ul>
  </div>
</nav>