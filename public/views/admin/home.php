<?php include '../../../config/session.php'; ?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    

    
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
    <!-- <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script> -->
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../css/loader_home.css">
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript">
        var cargar = 0;
        $(document).ready(function() {
            $('#datatable').DataTable();
        });
    </script>
    <script src="../../js/jquery.min.js"></script>
    <!-- <script src="../../js/bootstrap.js"></script> -->

</head>

<body>
    <?php include 'includes/navbar.php'; ?>

    <div class="d-flex justify-content-center mt-3 pa-5 ">
        <div class="card text-center w-90">
            <div class="card-header d-flex justify-content-between">
                <h3>
                    Campañas
                </h3>
                
                    <a onclick="readRecords();" class="btn btn-success">Activos</a>
                    <a onclick="readRecords2();" class="btn btn-info">Inactivos</a>
                        
                
                <!--<a href="resultados/formulario.php"><button type="button" class="btn btn-primary" id="ButtonModal" data-toggle="modal" data-target="#modalForm">-->
                <button type="button" class="btn btn-primary" id="ButtonModal" data-toggle="modal" data-target="#modalForm">
                    <i class="fa fa-plus"></i>
                </button>
            </div>
            <div class="card-body">
                <div class="col" id="Habilitados">
                    <div class="table-responsive">
                        <div>
                            <div id="contenedor_carga">
                            </div>
                            <div id="records_content"></div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="card-footer text-muted">
                <strong id="fechaActual"></strong>
            </div>
        </div>
    </div>
    <div class="modal fade" id="modalForm" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModal">Nuevo formulario</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="" method="POST" id="nuevo_formulario">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombre Campaña*</label>
                            <input type="text" name="nombre" maxlength="30" id="nombre" placeholder="Nombre Campaña" class="form-control">
                            <small class="form-text text-muted">Ingresa el nombre del formulario</small>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Url del Formulario*</label>
                            <input type="text" name="link_f" id="link_f" placeholder="Link del formulario" class="form-control">
                            <small class="form-text text-muted">Ingresa url del formulario</small>
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Url de los Resultados*</label>
                            <input type="text" name="link_r" id="link_r" placeholder="Aqui el link de respuesta" class="form-control">
                            <small class="form-text text-muted">Ingresa la url de los resultados</small>
                        </div>

                        <div class="form-group">
                            <label for="exampleTextarea">Descripción*</label>
                            <textarea class="form-control" name="descripcion" id="descripcion" placeholder="Descripción" rows="3"></textarea>
                        </div>
                        <!-- <input type="hidden" name="Registrar"> -->
                    </div>
                    <small id="msg"></small>
                    <div class="modal-footer">
                        <button type="su" id="guardar" class="btn btn-danger">Guardar</button>
                        
                        <button type="button" id="cancelar" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="modal fade" id="update_user_modal">
        <div class="modal-dialog">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <h4 class="modal-title">Actualizar formulario</h4>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <form action="" method="POST" id="editar_formulario">
                <!-- Modal body -->
                    <div class="modal-body">
                        <input type="hidden" id="edit_id" style="border: 1px solid black">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Nombre Campaña</label>
                            <input type="text" id="edit_nombre" name="edit_nombre" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Url del Formulario</label>
                            <input type="text" id="edit_link_f" name="edit_link_f" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="exampleInputEmail1">Url de los Resultados</label>
                            <input type="text" id="edit_link_r" name="edit_link_r" class="form-control">
                        </div>

                        <div class="form-group">
                            <label for="exampleTextarea">Descripción</label>
                            <textarea class="form-control" id="edit_descripcion" name="edit_descripcion" rows="3"></textarea>
                        </div>

                    </div>

                    <!-- Modal footer -->
                    <small id="msg_edit"></small>
                    <div class="modal-footer">
						<input type="submit" id="guardar_edit" class="btn btn-info" value="Guardar datos">
                        <input type="button" class="btn btn-secondary" data-dismiss="modal" value="Cancelar">
                    </div>
               </form>
            </div>
        </div>
    </div>

    

    <script>
        // function load(){
        //     setTimeout(function(){
        //         location.replace("read_sheet.php");
        // },1000);
        // }
        // window.onload = load();
    </script>

    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.18/dist/sweetalert2.all.min.js"></script>

    <script>
        $(document).ready(function() {
            readRecords();
        });
        // readRecords();

        $('#nuevo_formulario').submit(function(e){
            e.preventDefault()
            let nombreFormulario = $("#nombre").val();
            let urlFormulario = $("#link_f").val();
            let urlRespuestas = $("#link_r").val();
            let descripcion = $("#descripcion").val();

            let arrayUrlF = urlFormulario.split('https://docs.google.com/forms/d/')
            let arrayUrlR = urlRespuestas.split('https://docs.google.com/spreadsheets/d/')

            if(nombreFormulario == '' || urlFormulario == '' || urlRespuestas == '' || descripcion == ''){
                $('#msg').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Todos los campos son obligatorios.</div>");
                setTimeout(function(){
						$('#alerta').hide();
				},3000)
            }else if(arrayUrlF.length != 2){
                $('#msg').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Debe ser un URL de formulario de google valido.</div>");
                setTimeout(function(){
						$('#alerta').hide();
				},3000)
            }else if(arrayUrlR.length != 2){
                $('#msg').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Debe ser un URL de formulario de respuesta de google valido.</div>");
                setTimeout(function(){
						$('#alerta').hide();
				},3000)
            }else{
                $('#guardar').prop('disabled', true);
                $.ajax({
                    url: '../../../controllers/controlador_campana.php?action=save_campana',
                    type: 'POST',
                    method: 'POST',
                    data: {
                        nombreFormulario: nombreFormulario,
                        urlFormulario: urlFormulario,
                        urlRespuestas: urlRespuestas,
                        descripcion: descripcion,
                    },
                    success: function(resp) {
                        if(resp == 1){
                            $('#msg').html("<div class='alert alert-success' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Formulario agregado exitosamente.</div>");
                            setTimeout(function(){
                                location.replace('home.php');
                            },2000)
                        }else if(resp == 2){
                            $('#msg').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Todos los campos son obligatorios.</div>");
                            $('#guardar').prop('disabled', false);
                            setTimeout(function(){
                                    $('#alerta').hide();
                            },3000)
                        }else if(resp == 3){
                            $('#msg').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Sucedio un error con la base de datos intentalo mas tarde.</div>");
                            $('#guardar').prop('disabled', false);
                            setTimeout(function(){
                                    $('#alerta').hide();
                            },3000)
                        }else if(resp == 4){
                            $('#msg').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>El url de formulario ya existe por favor ingresa otro diferente.</div>");
                            $('#guardar').prop('disabled', false);
                            setTimeout(function(){
                                    $('#alerta').hide();
                            },3000)
                        }else if(resp == 5){
                            $('#msg').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>El url de resultados ya existe por favor ingresa otro diferente.</div>");
                            $('#guardar').prop('disabled', false);
                            setTimeout(function(){
                                    $('#alerta').hide();
                            },3000)
                        }
                        console.log(resp);
                    },
                })
            }
  

        })

        function addRecord() {

            var nombreFormulario = $("#nombreFormulario").val();
            var urlFormulario = $("#urlFormulario").val();
            var urlRespuestas = $("#urlRespuestas").val();
            var descripcion = $("#descripcion").val();

            $.ajax({

                url: "backend.php",
                type: 'POST',
                data: {
                    nombreFormulario: nombreFormulario,
                    urlFormulario: urlFormulario,
                    urlRespuestas: urlRespuestas,
                    descripcion: descripcion,
                },
                success: function(data, status) {
                    readRecords();
                },

            });

        }



        //////////////////Display Records activos
        function readRecords() {
            $("#contenedor_carga").html('<div id="carga"></div><p id="title-p">Cargando Campañas..</p>')
            $("#nombre").val("");
            $("#link_f").val("");
            $("#link_r").val("");
            $("#descripcion").val("");
            
            var readrecords = "readrecords";
            $.ajax({
                url: "backend.php",
                type: "POST",
                data: {
                    readrecords: readrecords
                },
                success: function(data, status) {
                    $("#contenedor_carga").css('display', 'none')
                    $('#records_content').html(data);
                },

            });
        }



        //////////////////Display Records inactivos
        function readRecords2() {
            //		alert();
            var readrecords2 = "readrecords2";
            $.ajax({
                url: "backend.php",
                type: "POST",
                data: {
                    readrecords2: readrecords2
                },
                success: function(data, status) {
                    $('#records_content').html(data);
                },

            });
        }

        //=============Delete Userdetails============
        function DeleteUser(deleteid) {

            Swal.fire({
            title: 'Estas seguro?',
            text: "Si continuas eliminaras el registro ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
            }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "backend.php",
                    type: 'POST',
                    data: {
                        deleteid: deleteid
                    },

                    success: function(data, status) {
                        Swal.fire(
                        'Eliminado!',
                        'El formulario fue eliminado con exito.',
                        'success'
                        )
                        readRecords();
                    }
                });
                
            }
            })

        }

        //=============Updatation Part===============
        //=============desactivar===============

        function DeleteUser2(deleteid2) {
            Swal.fire({
            title: 'Estas seguro?',
            text: "Desea desactivar el formulario?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Desactivar',
            cancelButtonText: 'Cancelar'
            }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "backend.php",
                    type: 'POST',
                    data: {
                        deleteid2: deleteid2
                    },

                    success: function(data, status) {
                        Swal.fire(
                        'Desactivado!',
                        'El formulario fue desactivado con exito.',
                        'success'
                        )
                        readRecords();
                    }
                });
                
            }
            })
        }

        function DeleteUser3(deleteid3) {

            Swal.fire({
            title: 'Estas seguro?',
            text: "Desea activar el formulario?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Activar',
            cancelButtonText: 'Cancelar'
            }).then((result) => {
            if (result.isConfirmed) {
                $.ajax({
                    url: "backend.php",
                    type: 'POST',
                    data: {
                        deleteid3: deleteid3
                    },

                    success: function(data, status) {
                        Swal.fire(
                        'Activado!',
                        'El Formulario fue Activado con exito.',
                        'success'
                        )
                        readRecords();
                    }
                });
                
            }
            })
        }

        $('#update_user_modal').on('show.bs.modal', function (event) {
		  var button = $(event.relatedTarget) // Button that triggered the modal
		  var id = button.data('id') 
		  $('#edit_id').val(id)
		  var name = button.data('name') 
		  $('#edit_nombre').val(name)
		  var urlf = button.data('urlf') 
		  $('#edit_link_f').val(urlf)
		  var urlr = button.data('urlr') 
		  $('#edit_link_r').val(urlr)
		  var descripcion = button.data('descripcion') 
		  $('#edit_descripcion').val(descripcion)

		})

        $('#editar_formulario').submit(function(e){
            e.preventDefault()

            let nombreFormulario = $("#edit_nombre").val();
            let urlFormulario = $("#edit_link_f").val();
            let urlRespuestas = $("#edit_link_r").val();
            let descripcion = $("#edit_descripcion").val();
            let id = $("#edit_id").val();


            let arrayUrlF = urlFormulario.split('https://docs.google.com/forms/d/')
            let arrayUrlR = urlRespuestas.split('https://docs.google.com/spreadsheets/d/')

            if(nombreFormulario == '' || urlFormulario == '' || urlRespuestas == '' || descripcion == ''){
                $('#msg_edit').html("<div class='alert alert-danger' id='alerta_edit' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Todos los campos son obligatorios.</div>");
                setTimeout(function(){
						$('#alerta_edit').hide();
				},3000)
            }else if(arrayUrlF.length != 2){
                $('#msg_edit').html("<div class='alert alert-danger' id='alerta_edit' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Debe ser un URL de formulario de google valido.</div>");
                setTimeout(function(){
						$('#alerta_edit').hide();
				},3000)
            }else if(arrayUrlR.length != 2){
                $('#msg_edit').html("<div class='alert alert-danger' id='alerta_edit' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Debe ser un URL de formulario de respuesta de google valido.</div>");
                setTimeout(function(){
						$('#alerta_edit').hide();
				},3000)
            }else{
                $('#guardar_edit').prop('disabled', true);
                $.ajax({
                    url: '../../../controllers/controlador_campana.php?action=update_campana',
                    type: 'POST',
                    method: 'POST',
                    data: {
                        nombreFormulario: nombreFormulario,
                        urlFormulario: urlFormulario,
                        urlRespuestas: urlRespuestas,
                        descripcion: descripcion,
                        id : id,
                    },
                    success: function(resp) {
                        if(resp == 1){
                            $('#msg_edit').html("<div class='alert alert-success' id='alerta_edit' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Formulario actualizado exitosamente.</div>");
                            setTimeout(function(){
                                location.replace('home.php');
                            },2000)
                        }else if(resp == 2){
                            $('#msg_edit').html("<div class='alert alert-danger' id='alerta_edit' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Todos los campos son obligatorios.</div>");
                            $('#guardar_edit').prop('disabled', false);
                            setTimeout(function(){
                                    $('#alerta_edit').hide();
                            },3000)
                        }else if(resp == 3){
                            $('#msg_edit').html("<div class='alert alert-danger' id='alerta_edit' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>Sucedio un error con la base de datos intentalo mas tarde.</div>");
                            $('#guardar_edit').prop('disabled', false);
                            setTimeout(function(){
                                    $('#alerta_edit').hide();
                            },3000)
                        }else if(resp == 4){
                            $('#msg_edit').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>El url de formulario ya existe por favor ingresa otro diferente.</div>");
                            $('#guardar_edit').prop('disabled', false);
                            setTimeout(function(){
                                    $('#alerta_edit').hide();
                            },3000)
                        }else if(resp == 5){
                            $('#msg_edit').html("<div class='alert alert-danger' id='alerta' style='padding-left: -2px;text-align: center;width: 90%;margin: auto;margin-bottom: 10px;'>El url de resultados ya existe por favor ingresa otro diferente.</div>");
                            $('#guardar_edit').prop('disabled', false);
                            setTimeout(function(){
                                    $('#alerta_edit').hide();
                            },3000)
                        }
                        console.log(resp)
                    },
                })
            }
  

        })

        function addRecord() {

            var nombreFormulario = $("#nombre").val();
            var urlFormulario = $("#link_f").val();
            var urlRespuestas = $("#link_r").val();
            var descripcion = $("#descripcion").val();

            $.ajax({

                url: "backend.php",
                type: 'POST',
                data: {
                    nombreFormulario: nombreFormulario,
                    urlFormulario: urlFormulario,
                    urlRespuestas: urlRespuestas,
                    descripcion: descripcion,
                },
                success: function(data, status) {
                    readRecords();
                },

            });

        }


        function getlink(url) {
        var aux = document.createElement("input");
        aux.setAttribute("value","https://prueba-token.herokuapp.com/public/views/index.php?codigo="+url);
        document.body.appendChild(aux);
        aux.select();
        document.execCommand("copy");
        document.body.removeChild(aux);
        $.ajax({

        success: function(data, status) {
            Swal.fire(
            'Url copiada',
            '¡Ahora ya puedes compartir la campaña!',
            'success'
            )
            readRecords();
        }
        });
        }
    </script>
    
    <!-- <script type="text/javascript" src="bootstrap.min.js"></script> -->

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>


</body>

</html>