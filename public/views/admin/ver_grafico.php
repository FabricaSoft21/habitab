<?php 
include '../../../config/session.php';
$_SESSION['nombre_tabla'] = $_GET['Campana'];
$e = explode("_",$_GET['Campana']);
?>

<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <script src="../../js/jquery.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.4/dist/Chart.min.js"></script>
    <link rel="stylesheet" href="../../css/grafico.css">
</head>

<body>
    <header>
        <?php include 'includes/navbar.php'; ?>
    </header>
    <section>
        <div class="container">
            <div class="d-flex justify-content-between">
                <h1 class="h1-title">Gráfica de la Campaña <?php echo $e[1]?></h1>
                <div style="width: 111px;padding-top: 15px;">
                    <a class="btn btn-outline-success my-2 my-sm-0" href="Lista_campanas.php">Volver</a>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-6 ">
                    <h3 style="text-align: center;">Género</h3>
                    <canvas id="grafica"></canvas>
                </div>
                <div class="col-sm-12 col-md-6 ">
                    <h3 style="text-align: center;">Estrato socioeconómico</h3>
                    <canvas id="grafica_2"></canvas>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-sm-12 col-md-6 ">
                <h3 style="text-align: center;">Rango de edad</h3>
                    <canvas id="grafica_3"></canvas>
                </div>
            </div>
                <!-- <div class="col-6">
                    <h3 style="text-align: center;">Estrato socioeconómico</h3>
                    <canvas id="grafica_2"></canvas>
                </div>
                <div class="col">
                    <h3 style="text-align: center;">Rango de edad</h3>
                    <canvas id="grafica_3"></canvas>
                </div> -->
            </div>
        </div>
    </section>
    <script type="text/javascript" src="graficas/G_Genero/script.js"></script>
    <script type="text/javascript" src="graficas/G_Estrato/script.js"></script>
    <script type="text/javascript" src="graficas/G_Edad/script.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>