<?php
require_once '../../../../../config/Conexion.php';
$Db = Dbs::Conectar();
session_start();
$nombre_tabla=$_SESSION["nombre_tabla"];

for($i=1;$i<=5;$i++){
    // $i=1;
    $SQL = $Db->prepare("SELECT COUNT(EstratoSocial) as Estrato FROM `$nombre_tabla` c INNER JOIN usuarios u ON c.usuario_id=u.id WHERE Genero='Masculino' AND EstratoSocial=$i");
    try {
        $SQL->execute();
        $Hombres = $SQL->fetch();
    } catch (Exception $e) //Capturar error 
    {
        echo $e->getMessage();
        die();
    }
    $datosH[] = $Hombres['Estrato'];
}

for ($i = 1; $i <= 5; $i++) {
    // $i=1;
    $SQL = $Db->prepare("SELECT COUNT(EstratoSocial) as Estrato FROM `$nombre_tabla` c INNER JOIN usuarios u ON c.usuario_id=u.id WHERE Genero='Femenino' AND EstratoSocial=$i ");
    try {
        $SQL->execute();
        $Mujeres = $SQL->fetch();
    } catch (Exception $e) //Capturar error 
    {
        echo $e->getMessage();
        die();
    }
    $datosM[] = $Mujeres['Estrato'];
}

// Valores con PHP. Estos podrían venir de una base de datos o de cualquier lugar del servidor
$etiquetas = ["Estrato 1","Estrato 2","Estrato 3","Estrato 4","Estrato 5"];
// Ahora las imprimimos como JSON para pasarlas a AJAX, pero las agrupamos
$respuesta = [
    "etiquetas" => $etiquetas,
    "datos" => $datosH,
    "datos2" => $datosM,
];
echo json_encode($respuesta);
