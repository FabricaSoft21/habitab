<?php
require_once '../../../../../config/Conexion.php';
$Db = Dbs::Conectar();
session_start();
$nombre_tabla = $_SESSION["nombre_tabla"];

$SQL = $Db->prepare("SELECT COUNT(*) as Hombres FROM `$nombre_tabla` c INNER JOIN usuarios u ON c.usuario_id=u.id WHERE Genero='Masculino'");
try {
    $SQL->execute();
    $Hombres = $SQL->fetch();
} catch (Exception $e) //Capturar error 
{
    echo $e->getMessage();
    die();
}
$datosH[] = $Hombres['Hombres'];



$SQL = $Db->prepare("SELECT COUNT(*) as Mujeres FROM `$nombre_tabla` c INNER JOIN usuarios u ON c.usuario_id=u.id WHERE Genero='Femenino'");
try {
    $SQL->execute();
    $Mujeres = $SQL->fetch();
} catch (Exception $e) //Capturar error 
{
    echo $e->getMessage();
    die();
}
$datosM[] = $Mujeres['Mujeres'];


$SQL = $Db->prepare("SELECT COUNT(*) as Otros FROM `$nombre_tabla` c INNER JOIN usuarios u ON c.usuario_id=u.id WHERE Genero='Otro'");
try {
    $SQL->execute();
    $Otro = $SQL->fetch();
} catch (Exception $e) //Capturar error 
{
    echo $e->getMessage();
    die();
}
$datosO[] = $Otro['Otros'];

// Valores con PHP. Estos podrían venir de una base de datos o de cualquier lugar del servidor
$etiquetas = ["Total respuestas"];
// Ahora las imprimimos como JSON para pasarlas a AJAX, pero las agrupamos
$respuesta = [
    "etiquetas" => $etiquetas,
    "datos" => $datosH,
    "datos2" => $datosM,
    "datos3" => $datosO,
];
echo json_encode($respuesta);
