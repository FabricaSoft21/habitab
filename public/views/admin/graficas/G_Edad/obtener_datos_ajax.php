<?php
require_once '../../../../../config/Conexion.php';
$Db = Dbs::Conectar();
session_start();
$nombre_tabla=$_SESSION["nombre_tabla"];
for($i=1;$i<=5;$i++){
    if($i==1){
        $j = "11 A 20";
    }elseif($i==2){
        $j = "21 A 30";
    }elseif($i==3){
        $j = "31 A 40";
    }elseif($i==4){
        $j = "41 A 50";
    }else{
        $j = "Mas de 50";
    }
    $SQL = $Db->prepare("SELECT COUNT(u.Edad) as Edades FROM `$nombre_tabla` c INNER JOIN usuarios u ON c.usuario_id=u.id WHERE Genero='Masculino' AND Edad='$j'");
    try {
        $SQL->execute();
        $Hombres = $SQL->fetch();
    } catch (Exception $e) //Capturar error 
    {
        echo $e->getMessage();
        die();
    }
    $datosH[] = $Hombres['Edades'];
}

for ($i = 1; $i <= 5; $i++) {
    if($i==1){
        $j = "11 A 20";
    }elseif($i==2){
        $j = "21 A 30";
    }elseif($i==3){
        $j = "31 A 40";
    }elseif($i==4){
        $j = "41 A 50";
    }else{
        $j = "Mas de 50";
    }
    $SQL = $Db->prepare("SELECT COUNT(u.Edad) as Edades FROM `$nombre_tabla` c INNER JOIN usuarios u ON c.usuario_id=u.id WHERE Genero='Femenino' AND Edad='$j' ");
    try {
        $SQL->execute();
        $Mujeres = $SQL->fetch();
    } catch (Exception $e) //Capturar error 
    {
        echo $e->getMessage();
        die();
    }
    $datosM[] = $Mujeres['Edades'];
}

// Valores con PHP. Estos podrían venir de una base de datos o de cualquier lugar del servidor
$etiquetas = ["11 A 20","21 A 30","31 A 40","41 A 50","Más de 50"];
// Ahora las imprimimos como JSON para pasarlas a AJAX, pero las agrupamos
$respuesta = [
    "etiquetas" => $etiquetas,
    "datos" => $datosH,
    "datos2" => $datosM,
];
echo json_encode($respuesta);