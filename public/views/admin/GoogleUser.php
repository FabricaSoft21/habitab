<?php include '../../../config/session.php'; ?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script>

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                "bDestroy": true,
                "iDisplayLength": 5, //Paginaci��n
                "order": [
                    [0, "desc"]
                ]
            });
        });
    </script>
</head>

<body>
    <?php include 'includes/navbar.php'; ?>

    <div class="d-flex justify-content-center mt-3 pa-5 ">
        <div class="card text-center w-90">
            <div class="card-header d-flex justify-content-between">
                <h1>USUARIOS DE GOOGLE </h1>

            </div>
            <div class="card-body">
                <?php
                
                $Db = Dbs::Conectar();
                $query = "SELECT * FROM `usuarios` where social='Google' order by id desc";
                $result = $Db -> query($query);
                ?>
                <?php if ($result->rowCount()>0) { ?>

                    <div class="table-responsive">
                        <table id="datatable" class="table">
                            <thead>
                                <tr>
                                    <th>id</th>
                                    <th>Nombre</th>
                                    <th>Correo</th>
                                    <th>Género</th>
                                    <th>Rango de edad</th>
                                    <th>Estrato socioeconómico</th>
                                    <th>Correo alternativo</th>
                                    <th>Red</th>
                                    <th>Icono</th>
                                </tr>
                            </thead>
                            <?php while ($row = $result->fetch()) { ?>
                                <tr>
                                    <td><?php echo $row['id']; ?></td>
                                    <td><?php echo $row['Nombre']; ?></td>
                                    <td><?php echo $row['Correo']; ?></td>
                                    <td><?php echo $row['Genero']; ?></td>
                                    <th><?php echo $row['Edad']; ?></th>
                                    <td><?php echo $row['EstratoSocial']; ?></td>
                                    <td><?php echo $row['Correo_alternativo']; ?></td>
                                    <td><?php echo $row['social']; ?> </td>
                                    <td><input type=image src="../../img/google.png" width="40" height="40"></input>
                                    </td>
                                </tr>
                            <?php } ?>
                        </table>
                    </div>

                <?php } ?>
            </div>
            <div class="card-footer text-muted">
            </div>
        </div>
    </div>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>