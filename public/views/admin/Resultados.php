<?php include '../../../config/session.php';?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="../../css/resultados.css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                "bDestroy": true,
                "iDisplayLength": 5, //Paginaci��n
                "order": [
                    [0, "desc"]
                ]
            });
        });
    </script>
</head>

<body>
    <header>
        <?php include 'includes/navbar.php'; ?>
    </header>
    <div class="d-flex justify-content-center mt-3 pa-5 ">
        <div class="card text-center w-90">
            <div class="card-header d-flex justify-content-between">
                <h1>Campañas </h1>
                <div style="width: 111px;padding-top: 15px;">
                    <a class="btn btn-outline-success my-2 my-sm-0" href="Lista_campanas.php">Volver</a>
                </div>
            </div>
            <div class="card-body">
                <?php

                
                $Db = Dbs::Conectar();
                $nameTable= $_GET['name'];
                $query = "SELECT * FROM $nameTable order by id DESC";
                $result = $Db -> query($query);
                ?>
                <?php if ($result->rowCount()>0) { ?>

                    <div class="table-responsive">
                    <table id="datatable" class="table">
                            <thead>
                                <tr>
                                    <?php
                                    foreach ($Db->query("SELECT count( COLUMN_NAME ) as columnas FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$nameTable' AND table_schema = ''") as $row) {
                                        $numerocolumnas = $row['columnas'];
                                    }

                                    foreach ($Db->query("SELECT * FROM $nameTable order by id ASC LIMIT 1") as $row) {
                                    ?>
                                        <th>id</th>
                                        <th>usuario</th>
                                        <th>correo</th>

                                    <?php
                                        for ($i = 1; $i <= $numerocolumnas - 3; $i++) {
                                    ?>        
                                        <th><p onclick="off_style(<?php echo $i?>)" id="style_<?php echo $i?>" class="style-questions"> <?php echo $row["Pregunta$i"] ?> </p></th>
                                    <?php
                                        }
                                    } ?>

                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($Db->query("SELECT * FROM $nameTable WHERE id!=1 ORDER BY id ASC") as $row) {
                                
                                    $idusuario=$row['usuario_id'];

                                    $name = $Db->query("SELECT Nombre FROM usuarios WHERE id=$idusuario");
                                    $userName = $name->fetch();
                                ?>
                                    <tr>
                                    <th><?php echo $row['id'] ?></th>
                                    <th><?php echo $userName['Nombre'] ?></th>
                                    <th><?php echo $row['correo'] ?></th>
                                <?php
                                    for ($i = 1; $i <= $numerocolumnas - 3; $i++) {
                                ?>
                                    <th><?php echo $row["Pregunta$i"] ?></th>
                                    
                                <?php
                                    }
                                    
                                } ?>
                                </tr>
                            </tbody>

                        </table>
                    </div>

                <?php } 
                $result = null;
                $Db = null;
                ?>
            </div>
            <div class="card-footer text-muted">
            </div>
        </div>
    </div>
<script>
    function off_style(val){
        if($('#style_'+val).hasClass("style-questions")){
            $('#style_'+val).removeClass("style-questions").addClass("style-questions-off");
        }else if($('#style_'+val).hasClass("style-questions-off")){
            $('#style_'+val).removeClass("style-questions-off").addClass("style-questions");
        }
    }
    

</script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>