<?php include '../../../config/session.php'; 

	if($_SESSION['rol'] != '1'){
		header('location: home.php');
	}

?>

<html lang="es">

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>H&B</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
	<script src="../../js/jquery.min.js"></script>
	<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
	<script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>


</head>

<body>

	<?php include 'includes/navbar.php'; ?>

	<div style="padding:10px;"></div>
	<div class="col-lg-12">
		<div class="card card-outline card-success">
			<div class="card-header">
				<div class="card-tools">
					<a class="btn btn-primary" href="new_user.php?page=new_user"><i class="fa fa-plus"></i> Nuevo Usuario</a>
				</div>
			</div>
			<div class="card-body">


				<div class="table-responsive">
					<table class="table tabe-hover table-bordered" id="list">
						<thead>
							<tr>
								<th class="text-center">#</th>
								<th>Nombre</th>
								<th>Username</th>

								<th>Email</th>
								<th>Acciones</th>
							</tr>
						</thead>
						<tbody>
							<?php
							$Db = Dbs::Conectar();
							$i = 1;
							$type = array('', "Admin", "Staff", "Subscriber");
							$query = $Db->query("SELECT *,concat(nombres,', ',apellidos) as name FROM usuarios_admin order by concat(nombres,', ',apellidos) asc");
							while ($row = $query->fetch(PDO::FETCH_ASSOC)) :
							?>
								<tr>
									<th class="text-center"><?php echo $i++ ?></th>
									<td><b><?php echo ucwords($row['name']) ?></b></td>
									<td><b><?php echo $row['username'] ?></b></td>

									<td><b><?php echo $row['email'] ?></b></td>
									<td class="text-center">

										<div >

											<a class="btn btn-warning" href="edit_user.php?page=edit_user&id=<?php echo $row['id'] ?>">Editar</a>

											<a class="btn btn-danger" onclick="delete_user(<?php echo $row['id'] ?>);" data-id="<?php echo $row['id'] ?>">Eliminar</a>
										</div>
									</td>
								</tr>
							<?php endwhile; ?>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.18/dist/sweetalert2.all.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#list').DataTable({
				"language": {
					"url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
				},
				"bDestroy": true,
				"iDisplayLength": 5, //Paginación
				"order": [
					[0, "desc"]
				]
			});
		});


		$(document).ready(function() {
			$('#list').dataTable()

			$('#delete_user').click(function() {
				_conf("Are you sure to delete this user?", "delete_user", [$(this).attr('data-id')])
			})
		})

		function delete_user($id) {
			Swal.fire({
            title: 'Estas seguro?',
            text: "Si continuas eliminaras el registro ",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Eliminar',
            cancelButtonText: 'Cancelar'
            }).then((result) => {
            if (result.isConfirmed) {
				$.ajax({
					url: 'ajax.php?action=delete_user',
					method: 'POST',
					data: {
						id: $id
					},
					success: function(resp) {
						if (resp == 1) {
							Swal.fire(
								'Eliminado!',
								'El formulario fue eliminado con exito.',
								'success'
							)

							setTimeout(function() {
								location.reload();
							}, 350)

						}
					}
				})
                
            }
            })

		}
	</script>
	<?php include 'footer.php' ?>