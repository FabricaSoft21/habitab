<?php include '../../../config/session.php';?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <script type="text/javascript" src="https://unpkg.com/xlsx@0.15.1/dist/xlsx.full.min.js"></script>
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <script src="../../js/jquery.min.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.15/i18n/Spanish.json"
                },
                "bDestroy": true,
                "iDisplayLength": 5, //Paginaci��n
                "order": [
                    [0, "desc"]
                ]
            });
        });
    </script>
</head>
<?php include 'includes/navbar.php'; ?>



<body>
    <?php


    $Db = Dbs::Conectar();
    $nameTable = $_GET['name'];


    $resultf = $Db->query("SELECT nombre_f FROM campana_maestra WHERE nombre_tbl_bd = '$nameTable'");
    $row = $resultf->fetch();

    ?>
    <div class="d-flex justify-content-center mt-3 pa-5 ">
        <div class="card text-center w-90">
        <div class="card-header d-flex justify-content-between">
                
                <h1>Campaña: <?php echo $row[0] ?>&nbsp;</h1>
                
                <div style="width: 111px;padding-top: 15px;">
                    <a class="btn btn-outline-success my-2 my-sm-0" href="Lista_campanas.php">Volver</a>
                </div>
            </div>

            <div class="card-header d-flex justify-content-between">
            <button onclick="ExportToExcel('xlsx')" class="btn btn-success">Exportar a Excel</button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <?php

                    $salida = "";
                    $salida .= "<table border='2' id='tbl_exporttable_to_xls' >";
                    // $salida .= "<thead>";
                    foreach ($Db->query("SELECT count( COLUMN_NAME ) as columnas FROM INFORMATION_SCHEMA.COLUMNS WHERE table_name = '$nameTable' AND table_schema = ''") as $row) {
                        $numerocolumnas = $row['columnas'];
                    }
                    foreach ($Db->query("SELECT * FROM $nameTable order by id ASC LIMIT 1") as $row) {
                        for ($i = 1; $i <= $numerocolumnas - 3; $i++) {
                            $preguntas = $row["Pregunta$i"];
                            $salida .= "<th colspan='2'>$preguntas</th>";
                        }
                    }
                    $salida .= "</thead>";
                    $salida .= "<tbody>";



                    $j = 0;
                    for ($i = 1; $i <= $numerocolumnas - 3; $i++) {
                        $salida .= "<th>Respuestas</th><th>N° respuestas</th>";
                        $user = $Db->query("SELECT count(*) total, Pregunta$i FROM $nameTable WHERE id!=1  GROUP BY Pregunta$i HAVING total>0");
                        if ($user->rowCount() > $j) {
                            $j = $user->rowCount();
                        }
                        while ($row = $user->fetch(PDO::FETCH_ASSOC)) {

                            $data[$i][] = $row;
                        }
                    }
                    //error_reporting(E_ALL ^ E_NOTICE);
                    $suma=0;
                    for ($k = 0; $k < $j; $k++) {
                        $salida .= "<tr>";
                        for ($i = 1; $i <= $numerocolumnas - 3; $i++) {
                            if (!isset($data[$i][$k])) {
                                $salida .= "<td ></td><td></td>";
                            } else {
                                $salida .= "<td >" . $data[$i][$k]['Pregunta' . $i] . "</td><td>" . $data[$i][$k]['total'] . "</td>";
                            }
                        }
                        $salida .= "</tr>";
                    }


                    $ancho=$numerocolumnas*2;
                    $salida .= " <tr><th colspan='".$ancho."'>MODAS</th></tr>";
                    $salida .= " <tr><th colspan='".$ancho."'>ESTRATO SOCIAL DEL 1 AL 3</th></tr>";
                    $salida .= "<tr>";
                    foreach ($Db->query("SELECT * FROM $nameTable order by id ASC LIMIT 1") as $row) {
                        for ($i = 1; $i <= $numerocolumnas - 3; $i++) {
                            $preguntas = $row["Pregunta$i"];
                            $salida .= "<th colspan='2'>$preguntas</th>";
                        }
                    }
                    $salida .= "</tr>";
                    $salida .= "<tr>";

                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $salida .= "<th>Respuestas</th><th>N° respuestas</th>";
                    }
                    $salida .= "</tr>";

                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        foreach ($Db->query("SELECT c.Pregunta$m, COUNT( c.Pregunta$m ) AS total FROM $nameTable c JOIN usuarios u ON c.usuario_id=u.id WHERE u.EstratoSocial=1 OR u.EstratoSocial=2 OR u.EstratoSocial=3 GROUP BY c.Pregunta$m ORDER BY total DESC LIMIT 1") as $key) {
                            $preguntamoda123=$key["Pregunta$m"];
                            $moda123= $key["total"];
                        }
                        $salida .= "<td>".$preguntamoda123."</td><td>".$moda123."</td>";
                    }
                    $salida .= "</tr>";
                    $salida .= " <tr><th colspan='".$ancho."'>ESTRATO SOCIAL DEL 4 AL 6</th></tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $salida .= "<th>Respuestas</th><th>N° respuestas</th>";
                    }
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        foreach ($Db->query("SELECT c.Pregunta$m, COUNT( c.Pregunta$m ) AS total FROM $nameTable c JOIN usuarios u ON c.usuario_id=u.id WHERE u.EstratoSocial=4 OR u.EstratoSocial=5 OR u.EstratoSocial=6 GROUP BY c.Pregunta$m ORDER BY total DESC LIMIT 1") as $key) {
                            $preguntamoda456=$key["Pregunta$m"];
                            $moda456= $key["total"];
                        }
                        if(isset($preguntamoda456))
                        $salida .= "<td>".$preguntamoda456."</td><td>".$moda456."</td>";
                    }
                    $salida .= "</tr>";
                    $salida .= " <tr><th colspan='".$ancho."'>MODA DE RESPUESTAS DE HOMBRES</th></tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $salida .= "<th>Respuestas</th><th>N° respuestas</th>";
                    }
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        foreach ($Db->query("SELECT c.Pregunta$m, COUNT( c.Pregunta$m ) AS total FROM $nameTable c JOIN usuarios u ON c.usuario_id=u.id WHERE u.Genero='Masculino' GROUP BY c.Pregunta$m ORDER BY total DESC LIMIT 1") as $key) {
                            $preguntamodaH=$key["Pregunta$m"];
                            $modaH= $key["total"];
                        }
                        $salida .= "<td>".$preguntamodaH."</td><td>".$modaH."</td>";
                    }
                    $salida .= "</tr>";
                    $salida .= " <tr><th colspan='".$ancho."'>MODA DE RESPUESTAS DE MUJERES</th></tr>";
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $salida .= "<th>Respuestas</th><th>N° respuestas</th>";
                    }
                    $salida .= "</tr>";
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        foreach ($Db->query("SELECT c.Pregunta$m, COUNT( c.Pregunta$m ) AS total FROM $nameTable c JOIN usuarios u ON c.usuario_id=u.id WHERE u.Genero='Femenino' GROUP BY c.Pregunta$m ORDER BY total DESC LIMIT 1") as $key) {
                            $preguntamodaM=$key["Pregunta$m"];
                            $modaM= $key["total"];
                        }
                        if(isset($preguntamodaM))
                        $salida .= "<td>".$preguntamodaM."</td><td>".$modaM."</td>";
                    }
                    $salida .= "</tr>";
                    $salida .= " <tr><th colspan='".$ancho."'>RANGO EDAD 11 A 30</th></tr>";
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $salida .= "<th>Respuestas</th><th>N° respuestas</th>";
                    }
                    $salida .= "</tr>";
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        foreach ($Db->query("SELECT c.Pregunta$m, COUNT( c.Pregunta$m ) AS total FROM $nameTable c JOIN usuarios u ON c.usuario_id=u.id WHERE u.Edad='11 A 20' OR u.Edad='21 A 30' GROUP BY c.Pregunta$m ORDER BY total DESC LIMIT 1") as $key) {
                            $preguntamodaM=$key["Pregunta$m"];
                            $modaM= $key["total"];
                        }
                        $salida .= "<td>".$preguntamodaM."</td><td>".$modaM."</td>";
                    }
                    $salida .= "</tr>";
                    $salida .= " <tr><th colspan='".$ancho."'>RANGO EDAD 31 A 50</th></tr>";
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $salida .= "<th>Respuestas</th><th>N° respuestas</th>";
                    }
                    $salida .= "</tr>";
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        foreach ($Db->query("SELECT c.Pregunta$m, COUNT( c.Pregunta$m ) AS total FROM $nameTable c JOIN usuarios u ON c.usuario_id=u.id WHERE u.Edad='31 A 40' OR u.Edad='41 A 50' GROUP BY c.Pregunta$m ORDER BY total DESC LIMIT 1") as $key) {
                            $preguntamodaM=$key["Pregunta$m"];
                            $modaM= $key["total"];
                        }
                        $salida .= "<td>".$preguntamodaM."</td><td>".$modaM."</td>";
                    }
                    $salida .= "</tr>";
                    $salida .= " <tr><th colspan='".$ancho."'>MEDIA</th></tr>";
                    $salida .= " <tr><th colspan='".$ancho."'>MEDIA DE LOS RESULTADOS OBTENIDOS</th></tr>";
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $salida .= "<th>Cantidad</th><th>Promedio</th>";
                    }
                    $salida .= "</tr>";
                    $salida .= "<tr>";
                    $suma=0;
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $cont=0;
                        foreach ($Db->query("SELECT count(Pregunta$m) total FROM $nameTable  GROUP BY Pregunta$m HAVING total>0") as $key) {
                            $cont++;
                            $total= $key["total"];
                            $suma=$suma + $total;
                        }
                        $promedio=$suma/$cont;
                        $salida .= "<td>".$suma."</td><td>".$promedio."</td>";
                        $suma=0;
                    }
                    $salida .= "</tr>";
                    $salida .= " <tr><th colspan='" . $ancho . "'>MEDIANA</th></tr>";
                    $salida .= " <tr><th colspan='" . $ancho . "'>MEDIANA DE LOS RESULTADOS OBTENIDOS</th></tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        $salida .= "<th>Respuesta</th><th>MEDIA</th>";
                    }
                    $salida .= "</tr>";
                    $salida .= "<tr>";
                    for ($m = 1; $m < $numerocolumnas - 2; $m++) {
                        foreach ($Db->query("SELECT count(Pregunta$m) total FROM $nameTable WHERE usuario_id!=1 GROUP BY Pregunta$m HAVING total>0 ORDER BY total DESC") as $key) {
                            $numeros[] = $key["total"];
                           // arsort($numeros);
                        }
                       // var_dump($numeros);
                        $cantidad=count($numeros);
                        $mitad=round($cantidad/2, 0, PHP_ROUND_HALF_DOWN);
                        foreach ($Db->query("SELECT pregunta$m,count(Pregunta$m) total FROM $nameTable WHERE usuario_id!=1 GROUP BY Pregunta$m HAVING total>0 AND total=$numeros[$mitad] ORDER BY total DESC") as $key) {
                            $preguntamediana = $key["pregunta$m"];
                        }
                        $salida .= "<td>".$preguntamediana."</td><td>" . $numeros[$mitad] . "</td>";

                        unset($numeros);
                    }
                    $salida .= "</tr>";
                    $salida .= "</table>";

                    echo $salida;


                    $resultf = $Db->query("SELECT nombre_f FROM campana_maestra WHERE nombre_tbl_bd = '$nameTable'");
                    $row = $resultf->fetch();
                    $resultf = null;
                    $Db = null; 
                    ?>
                </div>
                <div class="card-footer text-muted">
                </div>
            </div>

            <script>
                function ExportToExcel(type, fn, dl) {
                    var elt = document.getElementById('tbl_exporttable_to_xls');
                    var wb = XLSX.utils.table_to_book(elt, {
                        sheet: "sheet1"
                    });
                    return dl ?
                        XLSX.write(wb, {
                            bookType: type,
                            bookSST: true,
                            type: 'base64'
                        }) :
                        XLSX.writeFile(wb, fn || ('<?php echo $row[0], '.' ?>' + (type || 'xlsx')));
                }
            </script>

    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>

</body>

</html>