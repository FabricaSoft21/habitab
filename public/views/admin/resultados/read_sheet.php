<?php
require_once '../../../../config_sheet.php';
require_once '../../../../config/Conexion.php';
$Db = Dbs::Conectar();
foreach ($Db->query("SELECT codigo,nombre_tbl_bd FROM campana_maestra") as $row) {

    read_sheet($row['codigo'], $row['nombre_tbl_bd']);
    
}
$Db = null;
header("location: ../Lista_campanas.php");
function read_sheet($spreadsheetId = '', $nombretabla = '')
{

    $client = new Google_Client();

    $db = new DB();

    $arr_token = (array) $db->get_access_token();
    $accessToken = array(
        'access_token' => $arr_token['access_token'],
        'expires_in' => $arr_token['expires_in'],
    );

    $client->setAccessToken($accessToken);

    $service = new Google_Service_Sheets($client);


    try {
        $range = 'A:Z';
        $response = $service->spreadsheets_values->get($spreadsheetId, $range);
        $values = $response->getValues();


        if (empty($values)) {
            print "No data found.\n";
        } else {
            try {
                $conexion = Dbs::Conectar();
            } catch (PDOException $e) {
                exit("ERROR DE BASE DE DATOS");
            }
            /* check connection */
            // $conexion->query("TRUNCATE `bcs6mdxq3zz1pr4yt5ox`.`token`");
            $columnas = count($values[0]) - 1;
            //$columnas=2;
            include 'switchcreate.php';
            foreach ($conexion->query("SELECT count(*) as id FROM `$nombretabla`") as $row) {
                $max_id = $row['id'];
            }
            $reg_form = count($values) - 1;
            
            // echo "este es el numero de base de datos: $max_id";
            // echo "este es el numero del formulario: $reg_form";

            if ($max_id > $reg_form) :
                echo "Parece que la base de datos ya tiene los datos y algunos de mas, llame al soporte para verificar";
                $conexion = null;
            elseif ($max_id != $reg_form) :
                for ($i = $max_id; $i < count($values); $i++) {
                    $valor1=$values[$i][1];
                    $id_usuario = 0;
                    foreach ($conexion->query("SELECT `id` FROM `usuarios` WHERE Correo='$valor1' or Correo_alternativo='$valor1'") as $row) {
                        $id_usuario = $row['id'];
                    }
                    
                    if($id_usuario == 0){
                        $id_usuario = 1;
                    }
                    include 'switch.php';

                }
                $conexion = null;

            else :
                echo "Base de datos actualizada en el momento --- <br>";
                $conexion = null;

            endif;

            // echo "conexion exitosa <br>";
        //    header("Location: ../Lista_campanas.php");
        }
    } catch (Exception $e) {
        if (401 == $e->getCode()) {
            $refresh_token = $db->get_refersh_token();

            $client = new GuzzleHttp\Client(['base_uri' => 'https://accounts.google.com']);

            $response = $client->request('POST', '/o/oauth2/token', [
                'form_params' => [
                    "grant_type" => "refresh_token",
                    "refresh_token" => $refresh_token,
                    "client_id" => GOOGLE_CLIENT_ID,
                    "client_secret" => GOOGLE_CLIENT_SECRET,
                ],
            ]);

            $data = (array) json_decode($response->getBody());
            $data['refresh_token'] = $refresh_token;

            $db->update_access_token(json_encode($data));

            read_sheet($spreadsheetId, $nombretabla);
        } else {
            echo $e->getMessage(); //print the error just in case your video is not uploaded.
        }
    }
}

?>