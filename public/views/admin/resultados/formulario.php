<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">

    <script src="../../js/jquery.min.js"></script>
    <script src="../../js/jquery.js"></script>
    <script src="../../js/bootstrap.js"></script>
    <script src="../../js/bootstrap.min.js"></script>

    <script src="../../css/bootstrap.css"></script>
    <script src="../../css/bootstrap.min.css"></script>



    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>


</head>

<style>
    .content-1 {
        margin: 0 auto;
    }

    .content {

        display: flex;
        flex-direction: column;
        justify-content: center;
        width: 50%;
        background-color: #303030;
        padding: 5%;
    }
</style>

<body>


    <div class="content-1">
        <div class="content">
            <div class="header">
                <h5 class="title">Nuevo Campaña</h5>

            </div>
            <form align="center" action="../../../../controllers/controlador_campana.php" method="POST">

                <div class="body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Nombre Campaña</label>
                        <input type="text" name="nombre" maxlength="30" id="nombre" placeholder="Nombre Campaña" class="form-control">
                        <small class="form-text text-muted">Ingresa el nombre del formulario</small>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Url del Formulario</label>
                        <input type="text" name="link_f" id="link_f" placeholder="Link del formulario" class="form-control">
                        <small class="form-text text-muted">Ingresa url del formulario</small>
                    </div>

                    <div class="form-group">
                        <label for="exampleInputEmail1">Url de los Resultados</label>
                        <input type="text" name="link_r" id="link_r" placeholder="Aqui el link de respuesta" class="form-control">
                        <small class="form-text text-muted">Ingresa la url de los resultados</small>
                    </div>

                    <div class="form-group">
                        <label for="exampleTextarea">Descripción</label>
                        <textarea class="form-control" name="descripcion" id="descripcion" placeholder="Descripcion" rows="3"></textarea>
                    </div>
                    <input type="hidden" name="Registrar">
                </div>
                <div class="footer">

                    <button type="su" class="btn btn-danger">Guardar</button>
                    <button type="submit" id="Registrar" class="btn btn-secondary">Cerrar</button>
                </div>

            </form>

        </div>


    </div>

</body>

</html>