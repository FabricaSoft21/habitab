<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>ADMIN</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
</head>

<body>
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">Hábitat y Construcción</a>



        <div class="ml-auto">
            <ul class="navbar-nav mr-auto">

                <li class="nav-item">
                    <a class="nav-link" href="#">Facebook API</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="callback.php">Resultados</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="formulario.php">Registrar formularios</a>
                </li>
                &nbsp;
                <li class="nav-item">
                    <a class="btn btn-outline-success my-2 my-sm-0" href="#">salir <i class="fa fa-sign-out"></i></a>
                </li>
            </ul>
        </div>


    </nav>
</body>

</html>