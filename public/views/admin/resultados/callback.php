<?php
require_once '../../../../config_sheet.php';
  
try {
    $adapter->authenticate();
    $token = $adapter->getAccessToken();
    $db = new DB();
    if($db->is_table_empty()) {
        $db->update_access_token(json_encode($token));
        echo "Access token inserted successfully.";
    }
    header("Location: carga.php");
}
catch( Exception $e ){
    echo $e->getMessage() ;
}
