<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="../../../css/loader.css">

</head>
<body>
    <header class="backgroung-header">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3 pe-3">
                <a class="navbar-brand" href="#">Hábitat y Construcción</a>
        </nav>
    </header>
    <div id="contenedor_carga">
        <div id="carga"></div>
        <p id="title-p">Cargando Campañas..</p>
    </div>

    <script>
        function load(){
            setTimeout(function(){
                location.replace("read_sheet.php");
        },1000);
        }
        window.onload = load();
    </script>
    

<script>

</script>
</body>
</html>
