<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>H&B</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css"
          integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN"
          crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/validate_password.css">
        <style>
        @import "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css";
        </style>
        <script src="../js/jquery.min.js"></script>

    </head>
<body class="body-content">
    <header class="backgroung-header">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3 pe-3">
                <a class="navbar-brand" href="#">Hábitat y Construcción</a>
                <div class="ms-auto">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                                <a class="btn btn-primary" href="../../index.php" rel="noopener noreferrer">Inicio</a>
                        </li>
                    </ul>
                </div>  
        </nav>
    </header>
    <div class="d-flex justify-content-center mt-3 pa-5 ">
        <div class="card text-center w-50" id="bg-card">
            <div class="card-header">
                <strong>Restablecer Nueva Contraseña</strong>
            </div>
            <div class="card-body">
                <h5 class="card-title">Hábitat y Construcción</h5>
                <form action="" id="actualizar">
                        <input type="hidden" name="code" id="code" value="<?php if(isset($_GET['code'])) echo $_GET['code'] ?>">
                    <!-- prueba contraseña -->
						<label class="control-label" >Nueva Contraseña</label>
						<div class="center">
						<div class="form">
							<div class="form-element">
								
							<input type="password" class="form-control form-control-sm" id="password" name="password" autocomplete="off">
							<div class="toggle-password">
								<i class="fa fa-eye" style="color: grey;"></i>
								<i class="fa fa-eye-slash" style="color: grey;"></i>
							</div>
							<div class="password-policies">
								<div class="policy-length">
								Mínimo 8 caracteres
								</div>
								<div class="policy-number">
								Mínimo un número
								</div>
								<div class="policy-uppercase">
								Mínimo una mayúscula
								</div>
								<div class="policy-special">
								Al menos un carácter especial
								</div>
							</div>
							</div>
							
						</div>
						</div>
						<!-- finaliza prueba contraseña -->

						<!-- prueba contraseña -->
						<label class="control-label" >Confirmar Contraseña</label>
						<div class="center">
						<div class="form">
							<div class="form-element">
								
							<input type="password" class="form-control form-control-sm" id="cpass" name="cpass" >
							<div class="toggle-passwords">
								<i class="fa fa-eye" style="color: grey;"></i>
								<i class="fa fa-eye-slash" style="color: grey;"></i>
							</div>
							
							</div>
							
						</div>
						</div>
						<!-- finaliza prueba contraseña -->
                    <br>    
                    <button type="submit" class="btn btn-primary btn-block btn-flat" id="enviar" name="enviar"><i class="fa fa-sign-in"></i> Actualizar contraseña</button>
                    <small id="msg" data-status=''></small>
                </form>
            </div>
            <div class="card-footer text-muted">
                <?php 
                    if(isset($_GET['error'])){ 
                    echo $_GET['error'];
                    } 
                ?>
            </div>
        </div>
    </div>
  
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.18/dist/sweetalert2.all.min.js"></script>

<?php

if (!isset($_GET['code']) or $_GET['code'] == '') {
        echo "<script>
        Swal.fire({
            icon: 'error',
            title: 'No se encontro ningun codigo, te redireccionaremos al inicio',
            showConfirmButton: false,
            timer: 2500
        })
		$('#enviar').prop('disabled', true);
        setTimeout(function(){
            location.replace('../../index.php')
        },2500)
    </script>";
}
?>

<script>

$('[name="password"],[name="cpass"]').keyup(function(){
		var pass = $('[name="password"]').val()
		var cpass = $('[name="cpass"]').val()
		if(cpass == '' ||pass == ''){
			$('#pass_match').attr('data-status','')
		}else{
			if(cpass == pass){
				$('#pass_match').attr('data-status','1').html('<i class="text-success">Contraseña coincide.</i>')
			}else{
				$('#pass_match').attr('data-status','2').html('<i class="text-danger">La Contraseña no coincide.</i>')
			}
		}
	})

$('#actualizar').submit(function(e){
    e.preventDefault()
    let password = $('#password').val();
    let cpass = $('#cpass').val();
    let code = $('#code').val();

    var myregex = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}$/;
    if(password == '' || cpass == ''){
        $('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px; margin-top: 17px;'>Los campos son obligatorios.</div>");
        $('#msg').show();
        $('[name="password"]').addClass("border-danger")
        $('[name="cpass"]').addClass("border-danger")
        setTimeout(function(){
            $('#msg').hide();
        },3000)

    }else if(password != cpass){
      $('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px; margin-top: 17px;'>Las contraseñas no coinciden.</div>");
      $('#msg').show();
      $('[name="password"]').addClass("border-danger")
      $('[name="cpass"]').addClass("border-danger")
      $('#guardar').prop('disabled', false);
      setTimeout(function(){
          $('#msg').hide();
      },3000)

    }else if( myregex.test(password) & myregex.test(cpass)){
			$.ajax({
				url:'../../controllers/resetPassword.php',
				data: {
                    password : password,
                    cpass : cpass,
                    code: code
                },
				method: 'POST',
				type: 'POST',
				success:function(resp){
					if(resp == 1){
					$('#msg').html("<div class='alert alert-success' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;margin-top: 17px;'>Tu nueva contraseña fue registrada exitosamente.</div>");
						$('#msg').show();
						$('#enviar').prop('disabled', true);
						setTimeout(function(){
							location.replace('../../index.php')
                        },3000)
					}else if(resp == 2){
						$('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;margin-top: 17px;'>El codigo no fue enviado correctamente, vuelve a copiar url que se te envio al correo.</div>");
						$('#msg').show();
						$('#enviar').prop('disabled', true);
						setTimeout(function(){
							location.replace('../../index.php')
                        },3000)
          }else if(resp == 3){
              $('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;margin-top: 17px;'>El codigo no se encuentra en nuestras base de datos.</div>");
              $('#msg').show();
              setTimeout(function(){
                  $('#msg').hide();
              },3000)
          }else if(resp == 4){
              $('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;margin-top: 17px;'>Sucedio algun error con la base de datos.</div>");
              $('#msg').show();
              setTimeout(function(){
                  $('#msg').hide();
              },3000)
          }else if(resp == 5){
              $('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;margin-top: 17px;'>Las contraseñas no coinciden.</div>");
              $('#msg').show();
              $('[name="password"]').addClass("border-danger")
              $('[name="cpass"]').addClass("border-danger")
              setTimeout(function(){
                  $('#msg').hide();
              },3000)
          }else if(resp == 6){
              $('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;margin-top: 17px;'>Sucedio algun error en el sistema por favor contactar algun tecnico para solucionar el problema.</div>");
              $('#msg').show();
              setTimeout(function(){
                  $('#msg').hide();
              },3000)
          }else if(resp == 7){
              $('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px;margin-top: 17px;'>El codigo de recuperación de contraseña ya expiro, solicita uno nuevo.</div>");
              $('#msg').show();
              setTimeout(function(){
                  $('#msg').hide();
              },3000)
          }  
					console.log(resp)
				}
			})	
						
    }else{

    $('#msg').html("<div class='alert alert-danger' style='    width: 90%;margin: auto;text-align: center;font-size: 15px; margin-top: 17px;'>Elige una contraseña más segura.</div>");
    $('#msg').show();
    $('[name="password"]').addClass("border-danger")
    $('[name="cpass"]').addClass("border-danger")
    $('#guardar').prop('disabled', false);
    setTimeout(function(){
        $('#msg').hide();
    },3000)
  
    } 
})

function _id(name){
  return document.getElementById(name);
}
function _class(name){
  return document.getElementsByClassName(name);
}
_class("toggle-password")[0].addEventListener("click",function(){
  _class("toggle-password")[0].classList.toggle("active");
  if(_id("password").getAttribute("type") == "password"){
    _id("password").setAttribute("type","text");
  } else {
    _id("password").setAttribute("type","password");
  }
});

_class("toggle-passwords")[0].addEventListener("click",function(){
  _class("toggle-passwords")[0].classList.toggle("active");
  if(_id("cpass").getAttribute("type") == "password"){
    _id("cpass").setAttribute("type","text");
  } else {
    _id("cpass").setAttribute("type","password");
  }
});



_id("password").addEventListener("focus",function(){
  _class("password-policies")[0].classList.add("active");
});
_id("password").addEventListener("blur",function(){
  _class("password-policies")[0].classList.remove("active");
});

_id("password").addEventListener("keyup",function(){
  let password = _id("password").value;
  
  if(/[A-Z]/.test(password)){
    _class("policy-uppercase")[0].classList.add("active");
  } else {
    _class("policy-uppercase")[0].classList.remove("active");
  }
  
  if(/[0-9]/.test(password)){
    _class("policy-number")[0].classList.add("active");
  } else {
    _class("policy-number")[0].classList.remove("active");
  }
  
  if(/[^A-Za-z0-9]/.test(password)){
    _class("policy-special")[0].classList.add("active");
  } else {
    _class("policy-special")[0].classList.remove("active");
  }
  
  if(password.length > 7){
    _class("policy-length")[0].classList.add("active");
  } else {
    _class("policy-length")[0].classList.remove("active");
  }

  

});


</script>
 
</body>

<!-- Mirrored from www.ravijaiswal.com/Afro-v.1.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Mar 2017 03:30:10 GMT -->
</html>