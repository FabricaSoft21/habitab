<?php
require_once('../../config/Conexion.php');
session_start();

$Db = Dbs::conectar();
if(isset($_SESSION['email'])){
    $email = $_SESSION['email'];
    $sql = $Db->prepare("SELECT * FROM usuarios WHERE  Correo = ? ");
    $sql->execute([$email]);
    $result = $sql->rowCount();
    
    if($result != 0){
        header('location: campanas.php');
    }
}else{
    header('location: index.php');
}


$sql = null;
$Db = null;

?>
<!doctype html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>H&B</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
    <script src="../js/jquery.min.js"></script>
    <script src="../js/jquery.js"></script>
    <script src="../js/bootstrap.js"></script>
    <script src="../js/bootstrap.min.js"></script>

    <script src="../css/bootstrap.css"></script>
    <script src="../css/bootstrap.min.css"></script>
    <script src="../css/index.css"></script>


    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.css" />
    <script type="text/javascript" src="https://cdn.datatables.net/v/bs4/dt-1.10.18/datatables.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#datatable').DataTable();
        });
    </script>
    <link rel="stylesheet" href="../css/registro.css" />
    <!-- <link rel="stylesheet" type="text/css" href="../css/index.css" /> -->

</head>

<body>
    <div class="d-flex justify-content-center mt-3 pa-5 ">
        <div class="card text-center w-50">
            <div class="card-header d-flex justify-content-between">
                <h3>
                    Registro de datos
                </h3>
            </div>
            <div class="card-body">

                <form id="formulario" class="formulario" onsubmit="return agregaRegistroedadfacebook();">
                    <div class="form-group">


                        <input id="email" name="email" type="text" disabled value="<?php echo $_SESSION['email']; ?>">
                        <br>
                        <label for="exampleTextarea">Género</label>


                        <select class="form-control" style="text-align-last: center;" id="idgenero" name="idgenero">
                            <option>Masculino</option>
                            <option>Femenino</option>
                            <option>Otro</option>

                        </select>

                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Rango de edades</label>
                        <select class="form-control" style="text-align-last: center;" id="idedad" name="idedad">
                            <option>11 A 20 </option>
                            <option>21 A 30</option>
                            <option>31 A 40</option>
                            <option>41 A 50</option>
                            <option>Más de 50</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Estrato socioeconómico</label>
                        <select class="form-control" style="text-align-last: center;" id="estrato" name="estrato">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="exampleTextarea">Correo alternativo</label>
                        <div class="input-group">
                            <input class="form-control" style="text-align: center;" id="emailAlternative" name="emailAlternative" type="email" value="" aria-describedby="basic-addon1">
                            <div class="input-group-prepend tooltipde" id="div-tooltip" data-toggle="tooltip" title="Disabled tooltip">
                                <span class="tooltip-text">¡Ayúdanos a llegar más lejos! ingresa un correo de tu preferencia para mantenerte al día con nuestro proyecto.</span>
                                <span class="input-group-text" id="basic-addon1" tabindex="0" style="background-color: #2b55ef;">
                                    <i class="fa fa-exclamation" aria-hidden="true" style="color: #00ff89;"></i>
                                </span>
                    
                            </div>
                        
                        </div>
                    </div>

                    <input type="submit" value="Registrar" class="btn btn-primary" id="reg" />
                    <small id="msg_err" data-status=''></small>
                    <small id="mensaje" data-status=''></small>
                </form>

            </div>
        </div>
    </div>
    <script>
        function agregaRegistroedadfacebook() {
            $('#reg').prop('disabled', true);
            // alert();
            var url = 'registroUsuario/agregarUsuario.php';
            $.ajax({
                type: 'POST',
                url: url,
                data: $('#formulario').serialize(),
                success: function(registro) {
                    if(registro == 1){
                        $('#mensaje').html("<div class='alert alert-success' style='margin-top: 15px;text-align: center;font-size: 15px;'>Registro exitoso.</div>");
                        setTimeout(function(){
                            window.location.href = "registroUsuario/CorreoBien.php";
					    },750)
                    }else if(registro == 2){
                        $('#msg_err').html("<div class='alert alert-danger' style='margin-top: 15px;text-align: center;font-size: 15px;'>Este correo alternativo ya existe por favor ingresa otro.</div>");
                        $('#msg_err').show();
                        $('#reg').prop('disabled', false);
                        setTimeout(function(){
                            $('#msg_err').hide();
                        },3000)
                    }else{
                        $('#mensaje').addClass('bien').html('Sucedio algun error').show(200).delay(2500).hide(200);
                    }

                }
            });
            return false;
        }
    </script>