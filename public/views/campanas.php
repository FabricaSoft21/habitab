<?php
require_once('../../config/Conexion.php');
session_start();
// comprobar si tenemos los parametros id en la URL
//se pueden comprobar todas las que se deseen

if (isset($_GET["id"]) && isset($_GET["name"]) && isset($_GET["email"])) {
    // initialize session variables
    $_SESSION['id'] = $_GET["id"];
    $_SESSION['name'] = $_GET["name"];
    $_SESSION['email'] = $_GET["email"];
    $_SESSION['social'] = $_GET["social"];
    $email = $_SESSION['email'];

    $Db = Dbs::Conectar();
    $sql = $Db->prepare("SELECT * FROM usuarios WHERE  Correo = ? ");
    $sql->execute([$email]);
    $result = $sql->fetch();

    if ($result != false) {
        header("location: https://docs.google.com/forms/d/".$_SESSION['codigo']."/edit?usp=sharing");
    } else {
        header("Location: registroEdadGenero.php");
    }
} else {
    $email = $_SESSION['email'];
    $Db = Dbs::Conectar();
    $sql = $Db->prepare("SELECT * FROM usuarios WHERE  Correo = ? ");
    $sql->execute([$email]);
    $result = $sql->fetch();
    // $conteoResultado = $result->rowCount();
    // var_dump($result);
    if ($result != false) {
        header("location: https://docs.google.com/forms/d/".$_SESSION['codigo']."/edit?usp=sharing");
    } else {
        header("Location: registroEdadGenero.php");
    }
}


?>
<!-- 
<!doctype html>
<html lang="es">

<head>

    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>H&B</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet">
</head>

<body>
    <header class="backgroung-header">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3 pe-3">
            <a class="navbar-brand" href="#">Hábitat y Construcción</a>
            <
            if ($_SESSION['social'] == "Facebook") {
                echo "Bienvenido ", $_SESSION['name'], " haz iniciado sesion usando tu FACEBOOK";
            } elseif ($_SESSION['social'] == "Google") {
                echo "Bienvenido ", $_SESSION['name'], " haz iniciado sesion usando tu cuenta de GOOGLE";
            } else {
                echo ("No podemos identificar la red de la que inicio sesion, vuelva al iniciar");
            }

            ?>

            <div class="ml-auto">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item">

                        <a class="btn btn-primary" href="admin/logout.php" rel="noopener noreferrer">Cerrar sesión</a>

                    </li>
                </ul>
            </div>
        </nav>
    </header>

    <script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>

    <style>
        div.pagination {
            padding: 3px;
            margin: 3px;
            text-align: center;
            font-family: tahoma;
            font-size: 12px;
        }

        div.pagination a {
            padding: 2px 5px 2px 5px;
            margin: 2px;
            border: 1px solid #007799;
            text-decoration: none;
            color: #006699;
        }

        div.pagination a:hover,
        div.digg a:active {
            border: 1px solid #006699;
            color: #000;
        }

        div.pagination span.current {
            padding: 2px 5px 2px 5px;
            margin: 2px;
            border: 1px solid #006699;
            font-weight: bold;
            background-color: #006699;
            color: #FFF;
        }

        div.pagination span.disabled {
            padding: 2px 5px 2px 5px;
            margin: 2px;
            border: 1px solid #EEE;
            color: #DDD;
        }
    </style>

    <script type="text/javascript">
        $(document).ready(function() {
            change_page('0');
        });

        function change_page(page_id) {
            $(".flash").show();
            $(".flash").fadeIn(400).html('Cargando <img src="../img/ajax-loader.gif" />');
            var dataString = 'page_id=' + page_id;
            $.ajax({
                type: "POST",
                url: "registroUsuario/load_data.php",
                data: dataString,
                cache: false,
                success: function(result) {
                    $(".flash").hide();
                    $("#page_data").html(result);
                }
            });


            $('select').on('change', function() {

                $(".flash").show();
                $(".flash").fadeIn(400).html('Cargando <img src="../img/ajax-loader.gif" />');

                var d = document.getElementById('rutaUbi');
                var dato = d.options[d.selectedIndex].value;
                
                var url = 'registroUsuario/buscar2.php';
                $.ajax({
                    type: 'POST',
                    url: url,
                    data: 'dato=' + dato,
                    success: function(datos) {
                        $(".flash").hide();
                        $('#page_data').html(datos);
                    }
                });
                return false;

            })



        }
    </script>
    </head>

    <body>

        </div>
        <section>
            <table border="0" align="center">
                <tr>
                    </td>
                    <td width="400">
                        <div class="col-xs-9">

                            <select class="form-control" id="rutaUbi">
                                <option value="2">Ver Por Lotes De 2</option>
                                <option value="5">Ver Por Lotes De 5</option>
                                <option value="10">Ver Por Lotes De 10</option>
                                <option value="20">Ver Por Lotes De 20</option>
                                <option value="50">Ver Por Lotes De 50</option>
                                <option value="100">Ver Por Lotes De 100</option>
                            </select>
                    </td>

            </table>
        </section>
        <center>
            <div class='web'>
                <div class="registros" id="agrega-registros">
                    <div id="page_data">
                        <span class="flash"></span>
                    </div>
                </div>
            </div>
        </center>
    </body>

</html> -->