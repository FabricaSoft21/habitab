<?php include '../../controllers/requestReset.php' ?>

<html lang="es">
    <head>
        <meta charset="UTF-8">
        <title>H&B</title>
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
        <link rel="stylesheet"
          href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css"
          integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN"
          crossorigin="anonymous">
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
        <link rel="stylesheet" href="../css/index.css">
    </head>
<body class="body-content">
    <header class="backgroung-header">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3 pe-3">
                <a class="navbar-brand" href="#">Hábitat y Construcción</a>
                <div class="ms-auto">
                    <ul class="navbar-nav mr-auto">
                        <li class="nav-item">
                                <a class="btn btn-primary" href="../../index.php" rel="noopener noreferrer">Inicio</a>
                        </li>
                    </ul>
                </div>  
        </nav>
    </header>
    <div class="d-flex justify-content-center mt-3 pa-5 ">
        <div class="card text-center w-50" id="bg-card">
            <div class="card-header">
                <strong>Recuperar Contraseña</strong>
            </div>
            <div class="card-body">
                <h5 class="card-title">Hábitat y Construcción</h5>
                <form method="post">
                    <input class="form-control" type="text" name="email" placeholder="correo" autocomplete="off">
                    <br>
                    <button type="submit" class="btn btn-primary btn-block btn-flat" name="enviar"><i class="fa fa-sign-in"></i> Enviar</button>
                </form>
                <!-- <a href="../../index.php">Volver a inicio</a> -->
            </div>
            <div class="card-footer text-muted">
                <?php 
                    if(isset($_GET['codigo'])){ 
                    echo $_GET['codigo'];
                    } 
                ?>
            </div>
        </div>
    </div>

   


<script src="../../public/js/jquery.js"></script>
<script src="../../public/js/bootstrap.js"></script>
<script src="../../public/js/bootstrap.min.js"></script>


 
</body>

<!-- Mirrored from www.ravijaiswal.com/Afro-v.1.1/login.html by HTTrack Website Copier/3.x [XR&CO'2014], Sun, 19 Mar 2017 03:30:10 GMT -->
</html>