<?php
//error_reporting(E_ERROR | E_WARNING | E_PARSE);
require_once '../../loginGoogle.php';
session_start();
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-+0n0xVW2eSR5OomGNYDnhzAbDsOXxcvSN1TPprVMTNDbiYZCxYbOOl7+AMvyTG2x" crossorigin="anonymous">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootswatch/4.5.2/darkly/bootstrap.min.css" integrity="sha384-nNK9n28pDUDDgIiIqZ/MiyO3F4/9vsMtReZK39klb/MtkZI3/LtjSjlmyVPS3KdN" crossorigin="anonymous">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.3.1/css/all.css" integrity="sha384-mzrmE5qonljUremFsqc01SB46JvROS7bZs3IO2EmfFsd15uHvIt+Y8vEf7N7fWAU" crossorigin="anonymous">
    <link rel="stylesheet" href="public/css/loginUser.css">
    <title>H&B</title>
</head>

<body class="body-content">
    <!-- esta es una prueba para crear una rama -->
    <header class="backgroung-header">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark ps-3 pe-3">
            <a class="navbar-brand" href="#">Hábitat y Construcción</a>
        </nav>
    </header>
    <div class="d-flex justify-content-center mt-3 pa-5 ">
        <div class="card text-center w-50">
            <div class="card-header">
                <strong>Iniciar Sesión</strong>
            </div>
            <div class="card-body container">
                <h5 class="card-title">Hábitat y Construcción</h5>
                <p class="card-text">Inicia sesión para continuar con el formulario</p>

                <div class="btn-group">
                    <a class='btn btn-primary disabled'>
                        <i class="fab fa-facebook-f" class="icon-facebook"></i>
                    </a>
                    <a class='btn btn-primary btn-facebook' onclick="OnLogin()">
                        Iniciar sesión con facebook
                    </a>
                </div>
                <br /><br />
                <div class="btn-group">
                    <a class='btn btn-danger disabled'>
                        <i class="fab fa-google" class="icon-google"></i>
                    </a>
                    <?php echo "<a class= 'btn btn-danger btn-google'href='" . $client->createAuthUrl() . "' style=''>"; ?>
                    Iniciar sesión con Google
                    </a>
                    <!--Este es un comentario aleatorio-->
                </div>
            </div>
            <div class="card-footer text-muted">
                <strong id="fechaActual"></strong>
            </div>
        </div>
    </div>

    <input type="hidden" name="id" id="id">
    <input type="hidden" name="nombre" id="nombre">
    <input type="hidden" name="correo" id="correo">
    <input type="hidden" name="imagen" id="imagen">


</body>
<script src="../js/login.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.0.18/dist/sweetalert2.all.min.js"></script>
<script>
    function Alerta() {
        Swal.fire({
            icon: 'error',
            title: 'Error...',
            text: 'No se detecto un codigo en la URL, contacte el administrador y solicite una nueva URL',

        })
        setTimeout(function(){ history.back(); }, 3000);
    }


</script>
<?php
if (isset($_GET["codigo"])) {
    $_SESSION['codigo'] = $_GET["codigo"];
} else {
    echo "
        <script> Alerta(); </script>";
}
?>

</html>