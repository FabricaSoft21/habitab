<?php
class DB {

    private $dbHost     = "";
    private $dbUsername = "";
    private $dbPassword = "";
    private $dbName     = "";

    public function __construct(){
        if(!isset($this->db)){
            // Connect to the database
            $conn = new mysqli($this->dbHost, $this->dbUsername, $this->dbPassword, $this->dbName);
            if($conn->connect_error){
                die("Failed to connect with MySQL: " . $conn->connect_error);
            }else{
                $this->db = $conn;
            }
        }
    }
  
    public function is_table_empty() {
        
        $result = $this->db->query("SELECT id FROM token");
        if($result->num_rows) {
            $this->db->query("TRUNCATE TABLE token");
            return true;
        }
        return true;

    }
  
    public function get_access_token() {
        $sql = $this->db->query("SELECT access_token FROM token");
        $result = $sql->fetch_assoc();
        $this->db->close();

        return json_decode($result['access_token']);
    }
  
    public function get_refersh_token() {
        $result = $this->get_access_token();
        $this->db->close();
        return $result->refresh_token;
    }
  
    public function update_access_token($token) {
        if($this->is_table_empty()) {
            $this->db->query("INSERT INTO token(access_token) VALUES('$token')");
            $this->db->close();
        } else {
            $this->db->query("UPDATE token SET access_token = '$token' WHERE id = (SELECT id FROM token)");
            $this->db->close();

        }
    }

}