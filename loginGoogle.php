<?php
error_reporting(0);
//Precargamos la libreria de composer
require_once 'vendor/autoload.php';
include 'config/Conexion.php';

// Configuracion de la Api de Google (Credenciales)
$clientID = '';
$clientSecret = '';
$redirectUri = '';

//Creamos un metodo el cual asignamos los valores la Api y adicionamos la informacion del perfil
$client = new Google_Client();
$client->setClientId($clientID);
$client->setClientSecret($clientSecret);
$client->setRedirectUri($redirectUri);
//$client->addScope("id");
$client->addScope("email");
$client->addScope("profile");

//Validamos el codigo obtenido
if (isset($_GET['code'])) {
  $token = $client->fetchAccessTokenWithAuthCode($_GET['code']);
  $client->setAccessToken($token['access_token']);

  // Tomamos la informacion del perfil de Google
  $google_oauth = new Google_Service_Oauth2($client);
  $google_account_info = $google_oauth->userinfo->get();
  $id = $google_account_info->id;
  $email =  $google_account_info->email;
  $name =  $google_account_info->name;
  $social= "Google";

  // Estos datos son los que obtenemos y mostramos por pantalla....	
  //echo $id . '<br>';
  echo $email . '<br>';
  echo $name;

  // start a session
  session_start();
  // initialize session variables
  $_SESSION['id'] = $id;
  $_SESSION['name'] = $name;
  $_SESSION['email'] = $email;
  $_SESSION['social'] = $social;
  
      
  header("Location: public/views/campanas.php");
}
